<?php
//digital view
if(!class_exists('Aspk_Digital_Ecommerce_View')){
	class Aspk_Digital_Ecommerce_View{
		
		function Aspk_Digital_Ecommerce_View(){
			
			
		}
		function aspk_show_user_dashboard(){
			$css_class='selected_tab';
			if(isset($_GET['aspk_page'])){
			$current_page=$_GET['aspk_page'];
			}
			?>
			<div class="tw-bs container">
				<div class="row">
					<div class="digital_tab col-md-2 <?php if($current_page=='digital_inbox' || !isset($_GET['aspk_page']) ){ echo $css_class; }?>"> <a href="<?php echo home_url('/?aspk_page=digital_inbox');?>">Inbox</a>
					</div>
					<div class="digital_tab col-md-2 <?php if($current_page=='have_list') echo $css_class;?>" > 
					<a href="<?php echo home_url('/?aspk_page=have_list');?>">Have List</a>
					</div>
					<div class="digital_tab col-md-2 <?php if($current_page=='want_list') echo $css_class;?>" > 
					<a href="<?php echo home_url('/?aspk_page=want_list');?>">Want List</a>
					</div>
					<div class="digital_tab col-md-2 <?php if($current_page=='pending_trade') echo $css_class;?>" > <a href="<?php echo home_url('/?aspk_page=pending_trade');?>">Pending Trade</a>
					</div>
					<div class="digital_tab col-md-2 <?php if($current_page=='active_trade') echo $css_class;?>" > 
					<a href="<?php echo home_url('/?aspk_page=active_trade');?>">Active Trade</a>
					</div>
					<div class="digital_tab col-md-2 <?php if($current_page=='add_item') echo $css_class;?>"> 
					<a href="<?php echo home_url('?aspk_page=add_item');?>">Add Item to list</a>
					</div>
					<div class="digital_tab col-md-2 <?php if($current_page=='digital_calender') echo $css_class;?>"> <a href="<?php echo home_url('/?aspk_page=digital_calender');?>">Calendar</a>
					</div>
				</div>
			</div>
		<?php
		}
		
		function show_close_trade($id){ 
			?>
				<form method="post" action="">
					<div class="row">
						<div class="col-md-8" >
							<div style="float:left;"><img id="aspk_up_img_<?php echo $id; ?>" onclick="change_img_up_<?php echo $id; ?>();" src="<?php echo plugins_url('img/ThumbsUp.png', __FILE__);?>"></div>
							<div style="float:left;margin-left:2em;"><img id="aspk_down_img_<?php echo $id; ?>"  onclick="change_img_down_<?php echo $id; ?>();" src="<?php echo plugins_url('img/ThumbsDown.png', __FILE__);?>"></div>
						</div>
						<input type="hidden" value="" id="fb_val_adead33_<?php echo $id; ?>" name="aspk_fb_val">
						<input type="hidden" value="<?php echo $id; ?>"  name="aspk_ad_id_trade">
						<div class="col-md-4" >&nbsp;</div>
					</div>
					<div class="row">
						<div class="col-md-8" style="margin-top:1em;margin-bottom:2em;">
							<input type="submit" value="Close Deal" id="show_sub_btn_<?php echo $id; ?>" name="aspk_submit_close_d" class="btn btn-primary" style="display:none;">
							<input type="button" value="Close Deal" id="aspk_error_<?php echo $id; ?>" onclick="show_error_<?php echo $id; ?>();" class="btn btn-primary">
						</div>
						<div class="col-md-4" style="margin-top:1em;margin-bottom:2em;">&nbsp;</div>
					</div>
					<script>
						function change_img_up_<?php echo $id; ?>(){
							jQuery("#aspk_error_<?php echo $id; ?>").hide();
							jQuery("#show_sub_btn_<?php echo $id; ?>").show();
							jQuery("#fb_val_adead33_<?php echo $id; ?>").val(1);
							jQuery("#aspk_up_img_<?php echo $id; ?>").attr('disabled',true);
							jQuery("#aspk_up_img_<?php echo $id; ?>").attr('src','<?php echo plugins_url('img/thumbsup-after.png', __FILE__);?>');
							jQuery("#aspk_down_img_<?php echo $id; ?>").attr('src','<?php echo plugins_url('img/ThumbsDown.png', __FILE__);?>');
							jQuery("#aspk_down_img_<?php echo $id; ?>").attr('disabled',false);
						}
						function change_img_down_<?php echo $id; ?>(){
							jQuery("#aspk_error_<?php echo $id; ?>").hide();
							jQuery("#show_sub_btn_<?php echo $id; ?>").show();
							jQuery("#fb_val_adead33_<?php echo $id; ?>").val(2);
							jQuery("#aspk_down_img_<?php echo $id; ?>").attr('disabled',true);
							jQuery("#aspk_down_img_<?php echo $id; ?>").attr('src','<?php echo plugins_url('img/ThumbsDown-after.png', __FILE__);?>');
							jQuery("#aspk_up_img_<?php echo $id; ?>").attr('src','<?php echo plugins_url('img/ThumbsUp.png', __FILE__);?>');
							jQuery("#aspk_up_img_<?php echo $id; ?>").attr('disabled',false);
						}
						function show_error_<?php echo $id; ?>(){
							alert('Please select Thumbs up or Thumbs down for feedback');
						}
					</script>
				</form>
			<?php
		}
		
		function get_items($game){
			$args = array(
				'posts_per_page' => -1,
				'product_cat' => $game,
				'post_type' => 'product',
				'orderby' => 'title',
			);
			$the_query = new WP_Query( $args );
			// The Loop
			$retvals = array();
			while ( $the_query->have_posts() ) {
				$the_query->the_post();
				$retvals[] = get_the_id();
			}
			return $retvals;
		}
		
		function show_add_item(){
			?>
		<div class="tw-bs container" style="">
			<div class="row">
				<div class="col-md-12" >
					<form method="post" action="">
					<div class="row">
						<div class="col-md-8" >
							<div class="row">
								<div class="col-md-12" style="">
								<h1>Add item to List</h1>
								</div>
							</div>
							<div class="row">	
								<div class="col-md-4" style="">Game Name</div>
								<div class="col-md-8" style="">
									<select class="form-control" name="aspk_game" id="game_name" required>
									<option value="">Select Game</option>
									<?php
									$args = array(
										'number'     => '',
										'orderby'    => 'name',
										'order'      => 'ASC',
										'hide_empty' => 1
									);
									$product_categories = get_terms( 'product_cat', $args );
									foreach($product_categories as $cat){
										if($cat->slug == 'gold') continue;
										?>
										<option value="<?php echo $cat->slug;?>"><?php echo $cat->name;?></option>
										<?php 
										}?>
									</select>
								
								</div>
								<div class="col-md-3" style="margin-top:1em" id="aspk_feature_image10">
										
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-4" style="margin-top:1em">Item Name</div>
								<div id="item_name" class="col-md-8" style="margin-top:1em">
									<select class="form-control" name="aspk_item" disabled id="aspk_item" required>
										<option value="">Select</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-4" >&nbsp;</div>
					</div>
							<div  id="item_content" style="">
								<div class="row">
									<div class="col-md-8" >
										<div class="row">
											<div class="col-md-4" style="margin-top:1em">Description</div>
											<div class="col-md-8" style="margin-top:1em">
											<textarea class="form-control" name="aspk_description" rows="5" cols="3" style="height:10em"></textarea></div>
										</div>
										<div class="row">
											<div class="col-md-4" style="margin-top:1em">Asking Price</div>
											<div class="col-md-8" style="margin-top: 1em;">
													<input class="form-control" type="text" name="aspk_price" value="">	
											</div>
										</div>
										<div class="row">
											<div class="col-md-4" style="margin-top:1em">Tags</div>
											<div class="col-md-8" style="margin-top:1em">
											<input type="text" class="form-control" name="aspk_tags" value="" readonly/>
											</div>
										</div>
										
										<div class="row">
											<div class="col-md-4" style="margin-top:1em;">List</div>
											<div class="col-md-4" style="">
												<input type="submit" name="submit_want" value="Add to Want List" style="color:white;margin-top:1em;"/>
											</div>
											<div class="col-md-4" style="">
												<input type="submit" name="submit_have" value="Add to Have List" style="color:white;margin-top:1em;"/>
											</div>
										</div>	
									</div>	
									<div class="col-md-4" >
										<div  style="" id="aspk_feature_image10">
											<?php //echo get_the_post_thumbnail($product_id,'post-thumbnail');?>	
										</div>
									</div>
								</div>
							</div>
					</form>
				</div>
					
			</div>
				
		</div>		
	
			<script>
				jQuery(document).ready (function(){
					jQuery("#aspk_item121 a").attr('target', '_blank');
					jQuery("#aspk_tags a").attr ('href');
					jQuery("#aspk_img_src10").val(jQuery("#aspk_feature_image10 img").attr ('src'));
				jQuery( "#game_name" ).change(function(){
						jQuery("#aspk_item").attr('disabled',true);
						var game = jQuery( "#game_name option:selected" ).text();
						var data = {
						'action': 'aspk_digital_game',
						'aspk_game': game       
						};
						var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>'
						jQuery.post(ajaxurl, data, function(response) {
							obj = JSON.parse(response);
							 if(obj.st == 'ok'){
								 jQuery("#aspk_item").attr('disabled',false);
								jQuery('#aspk_item').find('option').remove().end();
								jQuery('#aspk_item').append(obj.html_reuslt);
							 }
						});
					});
				jQuery( "#aspk_item" ).change(function(){
						var item = jQuery( "#aspk_item option:selected" ).val();
						var data = {
						'action': 'aspk_digital_item',
						'digital_item': item       
						};
							var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>'
							jQuery.post(ajaxurl, data, function(response){
								obj = JSON.parse(response);
								 if(obj.st == 'ok'){
									 jQuery('#item_content').html('');
									jQuery('#item_content').append(obj.html_reuslt);
								 }
							 });
						});
					});
				</script>
		<?php	
		}

		function show_inbox_screen($results){
			if(count($results) < 1){
			?>
			<div class="tw-bs container" style="min-height:30em">
				<div class="row">
					<div class="col-md-12 digital_status">
					Sorry, you have no new message.
					</div>
				</div>
			</div>
			<?php
			}else {
			?>
			<div class="tw-bs container" style="min-height:30em">
				<?
				foreach( $results as $result){
					$id = $result->ad_id;
					$p_id = $result->product_id;
					$buyer_uid = $result->buyer_uid;
					$seler_uid = $result->seller_uid;
					$user_id = get_current_user_id();
					if($user_id == $seler_uid){
						$atr = '';
					}else{
						$atr = 'readonly';
					}
					$game_cat = $result->catagory_name;
					$price = get_post_meta( $p_id, '_regular_price',true );
					$date_time = date('Y-m-d H:i:s');
					$this->show_inbox_title($result);
				?>
				<div style="clear:left;display:none;" id="aspk_show_mail_<?php echo $id; ?>">
					<div class="row">
						<div class="col-md-2" style="margin-top:1em;">&nbsp;</div>
						<div class="col-md-8" style="margin-top:1em;"><?php echo $result->msg_flow; ?></div>
						<div class="col-md-2" style="margin-top:1em;">&nbsp;</div>
					</div>
					<div class="row">
						<div class="col-md-2" style="margin-top:1em;">&nbsp;</div>
						<div class="col-md-8" style="margin-top:1em;">
							<div style="background-color:#f5f5f5;padding:1em;clear:left;display:none;" id="aspk_final_<?php echo $id; ?>">
								<form method="post" action=""  >
									<div class="row">
										<div class="col-md-2" style="margin-top:1em;">
											<input type="hidden" value="<?php echo $buyer_uid; ?>" name="aspk_buyer_id_d">
											<input type="hidden" value="<?php echo $id; ?>" name="aspk_ad_id_d">
											<div style="float:left;"><b style="color:white;">Price:</b></div>
										</div>
										<div class="col-md-10" style="margin-top:1em;">
											<div style="float:left;"><input type="text" <?php echo $atr; ?> name="aspk_price_d" value="<?php echo $price; ?>"></div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-2" style="margin-top:1em;">
											<div style="float:left;"><b style="color:white;">Game:</b></div>
										</div>
										<div class="col-md-10" style="margin-top:1em;">
											<div style="float:left"><input type="text" readonly name="aspk_game_d" value="<?php echo $game_cat; ?>"></div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-10" style="margin-top:1em;">
											<input type="submit"  name="aspk_submit_deal" value="Confirm Deal" class="btn btn-primary">
										</div>
										<div class="col-md-2" style="margin-top:1em;"><input type="button" value="NO Thanks" class="btn btn-danger" onclick="hide_deal_form_<?php echo $id; ?>()"></div>
									</div>
								</form>
							</div>
						</div>
						<div class="col-md-2" style="margin-top:1em;">&nbsp;</div>
					</div>
					<form method="post" action="">
						<input type="hidden" value="<?php echo $id; ?>" name="aspk_ad_id_bila">
						<div class="row">
							<div class="col-md-2" style="margin-top:1em;">&nbsp;</div>
							<div class="col-md-8" style="margin-top:1em;"><textarea required  style="padding:1em;background-color:#6D9E95;border-radius:10px;" name="aspk_reply_conv" col="5" rows="3"></textarea></div>
							<div class="col-md-2" style="margin-top:1em;">&nbsp;</div>
						</div>
						<div class="row">
							<div class="col-md-5" style="margin-top:1em;">&nbsp;</div>
							<div class="col-md-7" style="margin-top:1em;margin-bottom:2em">
								<div style="float:left;"><input type="submit" value="Reply" name="aspk_reply_msg" style="width:9em;height:3em;" class="btn btn-primary"></div>
							<!--	<div style="float:left;margin-left:1em;"><input type="button" value="Finalize Deal" id="aspk_btn_deal_<?php echo $id; ?>" onclick="show_deal_form_<?php echo $id; ?>()" style="width:9em;height:3em;" class="btn btn-primary"></div>-->
							</div>
						</div>
					</form>
				</div>	
				<script>
					function show_deal_form_<?php echo $id; ?>(){
						jQuery("#aspk_final_<?php echo $id; ?>").show();
						jQuery("#aspk_btn_deal_<?php echo $id; ?>").hide();
					}
					function hide_deal_form_<?php echo $id; ?>(){
						jQuery("#aspk_final_<?php echo $id; ?>").hide();
						jQuery("#aspk_btn_deal_<?php echo $id; ?>").show();
					}
				</script>
				<?php
				}
				?>
			</div> <?php
			}
		}
		
		function show_inbox_title($result){
			$page_link = get_permalink();
			if(isset($_GET['aspk_page'])){
				$page=$_GET['aspk_page'];
			}
			$idd = $result->ad_id;
			$ad_title = $result->title;
			$b_user_id = $result->buyer_uid;
			$b_fname = get_user_meta( $b_user_id, 'first_name',true);
			$b_lname = get_user_meta( $b_user_id, 'last_name',true);
			$full_name = $b_fname.' '.$b_lname;
				?>
			<div class="row">
				<div class="col-md-10 col-sm-10" >
					<input type="button" style="margin-top:1em;background-color:#f5f5f5;padding:0.5em;
					border: 1px solid #d8d8d8;color: #1e1e1e;width: 60em;text-align: left;font-weight: bold;font-size:larger" onclick="aspk_show_mail_<?php echo $idd; ?>()" value="<?php echo $ad_title.' -- '.$full_name; ?>" >
				</div>
				<div class="col-md-2 col-sm-2" style="margin-top:1em;"><a href="<?php echo $page_link.'/?aspk_page='.$page.'&aspk_del_msgs='.$idd;?>"><img src="<?php echo plugins_url('/img/x.png',__FILE__); ?>"></a>
				</div>			
			</div>
			<script>
				function aspk_show_mail_<?php echo $idd; ?>(){
					jQuery("#aspk_show_mail_<?php echo $idd;?>").toggle();
				}
			</script>
		<?php
		}
		
		function show_active_trades($results){
			foreach($results as $r){
				$this->show_active($r);
			}
		}
		
		function show_active($rs){
			$product_id = $rs->product_id;
			$product = get_product( $product_id );
			$page_link = get_permalink();
			if( $rs->deal_buyer_id == get_current_user_id() ){
				$uu_id = $rs->user_id;
			}elseif( $rs->user_id == get_current_user_id() ){
				$uu_id = $rs->deal_buyer_id;
			}
			$up = get_user_meta($uu_id,'aspk_user_up_feedback',true);
			if(empty($up)) { $up = 0;}
			$down = get_user_meta($uu_id,'aspk_user_down_feedback',true);
			if(empty($down)) { $down = 0;}
			$user_data = get_userdata( $uu_id );
			$user_name = $user_data->user_nicename;
			?>
			<div class="tw-bs container" ><!-- start container -->
				<div class="row">
					<div class="col-md-2" style="margin-top:1em;">
						<?php echo get_the_post_thumbnail($product_id,'thumbnail');?>	
					</div>
					<div class="col-md-1" style="margin-top:1em"><?php echo $user_name; ?> </div>
					<div class="col-md-1" style="margin-top:1em">
						<div class="row">
							<div class="col-md-6">
								<img style="width:30px" src="<?php echo plugins_url('img/ThumbsUp',__FILE__); ?>"><?php echo '('.$up.')' ;?>
							</div>
							<div class="col-md-6">
								<img style="width:30px" src="<?php echo plugins_url('img/ThumbsDown',__FILE__); ?>" ><?php echo '('.$down.')' ;?>
							</div>
						</div>
					</div>
					<div class="col-md-4">	
						<div class="row">
							<div class="col-md-12"  ><h3><?php echo $product->post->post_title; ?></h3></div>
						</div>
						<div class="row">
							<div class="col-md-12"  >
								<div style="float:left;"><b>Price(g):</b></div>
								<div style="float:left;margin-left:1em;"><?php echo $rs->price ; ?></div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12"  ><p><?php echo $product->post->post_content;?></p></div>
						</div>
					</div>
					<div class="col-md-4">	
						<?php 
							$seller = $rs->seller_st;
							$buyer = $rs->buyer_st;
							if($seller == 0 || $buyer == 0 ){
								$this->show_close_trade($rs->adid); 
							}else{
								echo "<div class='row'><h2>Feedback given</h2></div>";
							}
							
						?>
					</div>
				</div>
			</div><!-- end container -->
			<?php
		}
		
		function show_record_items($results){
			foreach($results as $rs){
				$this->show_items($rs);
			}
		}
		
		function show_items($rs){
			$product_id = $rs->product_id;
			$product = get_product( $product_id );
			$page_link = get_permalink();
			if( $rs->buyer_userid == get_current_user_id() ){
				$uu_id = $rs->user_id;
			}elseif( $rs->user_id == get_current_user_id() ){
				$uu_id = $rs->buyer_userid;
			}
			$up=get_user_meta($uu_id,'aspk_user_up_feedback' , true);
			if(empty($up)) { $up = 0;}
			$down=get_user_meta($uu_id,'aspk_user_down_feedback' , true);
			if(empty($down)) { $down = 0;}
			$user_data=get_userdata( $uu_id );
			$user_name=$user_data->user_nicename;
			?>
			<div class="row">
				<div class="col-md-2" style="margin-top:1em;">
					<?php echo get_the_post_thumbnail($product_id,'thumbnail');?>	
				</div>
				<div class="col-md-3" style="margin-top:1em">
					<div style="float:left;margin-left:10px;">	<?php echo $user_name; ?> </div>
					<div style="float:left;margin-left:10px;">
						<div style="clear:left;">
							<img style="width:30px" src="<?php echo plugins_url('img/ThumbsUp.png',__FILE__); ?>">
						</div>
						<div style="clear:left;"><?php echo '('.$up.')' ;?></div>
					</div>
					<div style="float:left;margin-left:10px;">
						<div style="clear:left;">
							<img style="width:30px" src="<?php echo plugins_url('img/ThumbsDown.png',__FILE__); ?>" >
						</div>
						<div style="clear:left;"><?php echo '('.$down.')' ;?></div>
					</div>
				</div>
				<div class="col-md-7 make_offer">	
					<div style="float:left;">
						<form method="post" action="" style="display:none;" id="aspk_form_<?php echo $rs->id; ?>">
							<div class="row">
								<input type="hidden" value="<?php echo $rs->adid; ?>" name="aspk_ad_id">
								<input type="hidden" value="<?php echo $rs->user_id; ?>" name="aspk_seler_name">
								<input type="hidden" value="<?php echo $rs->buyer_userid; ?>" name="aspk_buyer_name">
								<div class="col-md-12"  style="margin-top:1em" ><textarea required name="aspk_buyer_message" rows="5" cols="10"></textarea></div>
							</div>
							<div class="row">
								<div class="col-md-6" style="margin-top:1em" ><input type="submit" value="Send" class="btn btn-primary" name="aspk_submit_msgrr21" /></div>
								<div class="col-md-6" style="margin-top:1em" ><input type="button" value="No Thanks" class="btn btn-danger" onclick="hide_form_<?php echo $rs->id; ?>();" /></div>
							</div>
						</form>
						<div class="row">
							<div class="col-md-12"  ><h3><?php echo $product->post->post_title; ?></h3></div>
						</div>
						<div class="row">
							<div class="col-md-12" ><div style="float:left;"><b>Offered Price(g)</b></div>
								<div style="float:left;margin-left:10px;"><?php echo $rs->ofr_price; ?></div>
							</div>
						</div>
					</div>
					<div style="float:left;margin-left:10px;">
						<input type="button" value="Send Message" class="btn btn-primary btn_offer" id="aspk_send_<?php echo $rs->id; ?>" onclick="show_msg_form_<?php echo $rs->id; ?>();" />
					</div>
					<?php 
					if( $rs->user_id == get_current_user_id() ){
					?>
					<div style="float:left;margin-left:10px;">
						<input type="button" class="btn btn-primary btn_offer" value="Accept Offer" id="aspk_accept_<?php echo $rs->id; ?>" style="margin-right:5px;" onclick="show_accept_form_<?php echo $rs->id; ?>();">
						<a href="<?php echo $page_link.'?aspk_page=pending_trade&aspk_del_ad='.$rs->id ?>"><img src="<?php echo plugins_url('img/x.png', __FILE__);?>"></a>
					</div>
					<div class="col-md-6">
						<div  style="display:none;" id="aspk_accept_form_<?php echo $rs->id; ?>">
							<form method="post"  action="" >
								<input type="hidden" value="<?php echo $rs->adid; ?>" name="agile_ad_id">
								<input type="hidden" value="<?php echo $rs->buyer_userid; ?>" name="agile_buyer_id">
								<input type="hidden" value="<?php echo $rs->user_id; ?>" name="agile_seler_id">
								<input type="hidden" value="<?php echo $rs->ofr_price; ?>" name="agile_price">
								<div style="clear:left;"><b>Date/Time</b></div>
								<div style="float:left;">
									<input required type="text" style="width:6em;" id="datepicker_<?php echo $rs->id; ?>" name = "aspk_date" value = "" >
									<select name="aspk_hr" style="width:4em;" required>
										<option value="">hr</option>
										<?php
										for($hr =0; $hr <=23 ; $hr++){ ?>
											<option value="<?php echo $hr; ?>"><?php echo $hr; ?></option> <?php
										}
										?>
									</select>
									<select name="aspk_time" style="width:5em;" required>
										<option value="">mnt</option>
										<?php
										for($time =0; $time <=59 ; $time++){ ?>
											<option value="<?php echo $time; ?>"><?php echo $time; ?></option> <?php
										}
										?>
									</select>
								</div>
								<div style="clear:left;">
									<input style="margin-top:10px;" type="submit" class="btn btn-primary btn_offer" value="Accept" name="aspk_accept_offer">
								</div>
								<!--<div style="float:left;margin-top:10px;margin-left:10px;">
									<input type="button" class="btn btn-danger" value="No Thanks"  onclick="show_ignore_form_<?php echo $rs->id; ?>();">
								</div>-->
							</form>
						</div>
					</div>
					<?php 
					}
					?>
				</div>
			</div>
			<script>
			
				jQuery(document).ready(function(){
						jQuery( "#datepicker_<?php echo $rs->id; ?>" ).datepicker();
					});
			
				function show_msg_form_<?php echo $rs->id; ?>(){
					jQuery("#aspk_send_<?php echo $rs->id; ?>").hide();
					jQuery("#aspk_form_<?php echo $rs->id; ?>").show();
				}
				
				function hide_form_<?php echo $rs->id; ?>(){
					jQuery("#aspk_send_<?php echo $rs->id; ?>").show();
					jQuery("#aspk_form_<?php echo $rs->id; ?>").hide();
				}
				
				function show_accept_form_<?php echo $rs->id; ?>(){
					jQuery("#aspk_accept_<?php echo $rs->id; ?>").hide();
					jQuery("#aspk_accept_form_<?php echo $rs->id; ?>").show();
				}
				
				function show_ignore_form_<?php echo $rs->id; ?>(){
					jQuery("#aspk_accept_<?php echo $rs->id; ?>").show();
					jQuery("#aspk_accept_form_<?php echo $rs->id; ?>").hide();
					jQuery("#datepicker_<?php echo $rs->id; ?>").val('');
				}
				
			</script>
			<?php
		}
		
		function aspk_show_list_view($all_ads,$want_list_count_ads,$game_values,$end_limit){
			$page_link = get_permalink();
			$user_id = get_current_user_id();
			if($all_ads){
				foreach($all_ads as $ad){
					$product_id=$ad->product_id;
					$product = get_product( $product_id );
					$up=get_user_meta($ad->user_id,'aspk_user_up_feedback',true);
					if(empty($up)) { $up = 0;}
					$down=get_user_meta($ad->user_id,'aspk_user_down_feedback',true);
					if(empty($down)) { $down = 0;}
					$user_data=get_userdata( $ad->user_id );
					$user_name=$user_data->user_nicename;
				?>
					<div class="tw-bs container" style="">
						<div class="row">
							<div class="col-md-2" style="margin-top:1em;">
								<?php echo get_the_post_thumbnail($product_id,'thumbnail');?>	
							</div>
							<div class="col-md-1" style="margin-top:1em"><?php echo $user_name; ?> </div>
							<div class="col-md-2" style="margin-top:1em">
								<div style="float:left;">
									<img style="width:30px" src="<?php echo plugins_url('img/ThumbsUp.png',__FILE__); ?>"><br/><?php echo '('.$up.')' ;?>
								</div>
								<div style="float:left;margin-left:1em;">
									<img style="width:30px" src="<?php echo plugins_url('img/ThumbsDown.png',__FILE__); ?>" ><br/><?php echo '('.$down.')' ;?>
								</div>
							</div>
							<div class="col-md-4">
								<form method="post" action="">
									<div class="make_offer" style="display:none;" id="aspk_show_ofr_<?php echo $ad->adid; ?>">
										<div class="row">
											<div class="col-md-12" >
												<div style="float:left;"><b>Offered Price(g)</b></div>
												<div style="float:left;margin-left:1em;">
													<input type="text" required value="" name="agile_price">
													<input type="hidden" value="<?php echo $ad->adid; ?>" name="aspk_ad_id_ofr">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-8" style="margin-top:1em;">
												<input type="submit" class="btn btn-primary btn_offer" name="agile_submit_ofer" value="Make Offer">
											</div>
											<div class="col-md-4" style="margin-top:1em;">
												<input type="button"  value="No Thanks" onclick="hide_offer_<?php echo $ad->adid; ?>();" class="btn btn-danger">
											</div>
										</div>
									</div>
								</form>
								<div class="row">
									<div class="col-md-12" style="font-size:40px;" ><a href="<?php echo $page_link.'?pid='.$product_id.'/&act='.'detail_page';?>"><?php echo $product->post->post_title; ?></a>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12"  ><p><?php echo $product->post->post_content;?></p></div>
								</div>
								<div class="row">
									<div class="col-md-12" style="font-size:20px;" ><?php echo $product->regular_price .'g'; ?></div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="row">
									<div class="col-md-12">
										<input type="button" class="btn btn-primary btn_offer" id="aspk_btn_ofr_<?php echo $ad->adid; ?>" value="Make Offer" onclick="show_offer_div_<?php echo $ad->adid; ?>();">
									</div>
								</div>
							</div>
						</div>
						<script>
							function show_offer_div_<?php echo $ad->adid; ?>(){
								jQuery("#aspk_btn_ofr_<?php echo $ad->adid; ?>").hide();
								jQuery("#aspk_show_ofr_<?php echo $ad->adid; ?>").show();
							}
							
							function hide_offer_<?php echo $ad->adid; ?>(){
								jQuery("#aspk_btn_ofr_<?php echo $ad->adid; ?>").show();
								jQuery("#aspk_show_ofr_<?php echo $ad->adid; ?>").hide();
							}
						</script>
						<div id="more_results">
						</div>
					</div>
			<?php 
				}
				if($want_list_count_ads >= 25){
						?><div class="row">
							<div class="col-md-10">
							<input class="btn btn-primary" type="button" id="aspk_load" value="Load more" onclick="show_more()" style="font-size: 25px;padding-right: 2em;padding-left: 2em;margin-left: 30em;margin-left:19em;margin-bottom: 1em;"/>
							<input type="hidden" id="aspk_last_val121" value="<?php echo $end_limit; ?>"/>
							</div>
						</div>
						<?php 
				}
				?>
				<script>
				function show_more(){
					var digi_end_limit = jQuery("#aspk_last_val121").val();
					var digi_game = <?php echo $game_values;?>;
					var digi_user_id = <?php echo $user_id ;?>;
					var data = {
					'action': 'aspk_load_more',
					'aspk_end_limit': digi_end_limit,
					'aspk_game':digi_game,
					'aspk_user_id':digi_user_id
					};
					var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>'
					jQuery.post(ajaxurl, data, function(response) {
						obj = JSON.parse(response);
							if(obj.st == 'ok'){
							jQuery('#more_results').append(obj.html_result); 
							jQuery("#aspk_last_val121").val(obj.last_id);
							}
						
					});
				}
			</script>
			<?php
			}
		}
		
		function product_list_show($all_product_list){
			$page_link = get_permalink();
			if(isset($_GET['aspk_page'])){
				$page=$_GET['aspk_page'];
			}
			$user_id=get_current_user_id();
			$user_balance=get_usermeta($user_id,'_aspk_de_bal');
			$user_amount= $user_balance['amount'];
			if(empty ($user_amount)){
				$user_amount='0';
			}
			?>
			<div class="tw-bs container" style="min-height:30em">
				<div class="row">
					<div class="col-md-12" style="float:right;font-size:20px;margin-top:1em;">
						Current Balance: <span style="color:green;font-weight: bold;"><?php echo $user_amount.'g';?></span>
					</div>
				</div>
			<?php
			
			if($all_product_list){
				foreach($all_product_list as $ad){
					$product_id=$ad->product_id;
					$product = get_product( $product_id );
				?>
				<div class="row">
					<div class="col-md-3" style="float:left;margin-right:3em;width:13em;">
						<?php echo get_the_post_thumbnail($product_id,'thumbnail');?>	
					</div>
					<div class="col-md-7">
						<div class="row">
							<div class="col-md-12" style="font-size:30px;margin-top:1em;" ><a href="<?php echo $page_link.'?aspk_page='.$page.'&pid='.$product_id.'&act='.'detail_page';?>"><?php echo $product->post->post_title; ?></a>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 list_view" ><?php echo $product->post->post_content;?></div>
						</div>
						<div class="row">
							<div class="col-md-12 list_view" style="font-size:20px;margin-top:10px" ><?php echo $product->regular_price.'g'; ?></div>
						</div>
					</div>
					<div class="col-md-1" style="margin-top:2em;"><a href="<?php echo $page_link.'?aspk_page='.$page.'&aspk_del_ad='.$ad->adid ?>"><img src="<?php echo plugins_url('img/x.png', __FILE__);?>"></a>
					</div>
					<div class="col-md-1">&nbsp;</div>
				</div>
		<?php   } ?>
			</div>
		<?php
			}
		}
		
		function aspk_show_detail_view_to_current_user($single_ad){
			$uid = $single_ad->user_id;
			$fname = get_user_meta( $uid, 'first_name',true);
			$lname = get_user_meta( $uid, 'last_name',true);
			$full_name = $fname.' '.$lname;
			$b_uid = get_current_user_id();
			$b_fname = get_user_meta( $b_uid, 'first_name',true);
			$b_lname = get_user_meta( $b_uid, 'last_name',true);
			$buyer_name = $b_fname.' '.$b_lname;
			$product_id=$single_ad->product_id;
			$product = get_product( $product_id );
			$author_id=$product->post->post_author;
			$user_data=get_userdata( $author_id );
			$user_email=$user_data->user_email;
			?>
			<style>
			
			.aspk_label{
				font-size:20px;
			}
			.aspk_value{
				font-size:20px;
				color:#1FA67A;
			}
			#send_message{
				margin-top:1em;
			}
			</style>
			<div class="tw-bs container" style="margin-top: 2em;">
				<div class="row">
					<div class="col-md-12" style="font-size:40px;" ><?php echo $single_ad->title;?></div>
				</div>
				<div class="row">
					<div class="col-md-5">
						<div class="row">
							<div class="col-md-12">
								<?php echo get_the_post_thumbnail($product_id,'large' );?>	
							</div>
						</div>
					</div>
					<div class="col-md-7" style="margin-top:2em;">
						<div class="row">
							<div class="col-md-4 aspk_label" style="margin-top:1em;">Game </div>
							<div class="col-md-8 aspk_value" style="margin-top:1em;" >
								<?php
									$cat = str_replace("-"," ",$single_ad->catagory_slug );
									echo ucwords($cat);
								?>
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-4 aspk_label" style="margin-top:1em;">Price </div>
							<div class="col-md-8 aspk_value" style="margin-top:1em;"><?php echo $product->get_price_html().'g'; ?></div>
						</div>
						<div class="row">
							<div class="col-md-4 aspk_label" style="margin-top:1em;" >Catagory</div>
							<div class="col-md-8 aspk_value" style="margin-top:1em;" ><?php echo ucwords($cat);?></div>
						</div>
					</div>
				</div>
				<div class="row">
				<div class="col-md-6"><h2>Description<h2></div>
					<div class="col-md-10 aspk_label" ><?php echo $single_ad->description;?></div>
				</div>
			</div>
		<?php	
		}
		
		function aspk_show_detail_view($single_ad){
			$uid = $single_ad->user_id;
			$fname = get_user_meta( $uid, 'first_name',true);
			$lname = get_user_meta( $uid, 'last_name',true);
			$full_name = $fname.' '.$lname;
			$b_uid = get_current_user_id();
			$b_fname = get_user_meta( $b_uid, 'first_name',true);
			$b_lname = get_user_meta( $b_uid, 'last_name',true);
			$buyer_name = $b_fname.' '.$b_lname;
			$product_id=$single_ad->product_id;
			$product = get_product( $product_id );
			$author_id=$product->post->post_author;
			$user_data=get_userdata( $author_id );
			$user_email=$user_data->user_email;
			?>
			<style>
			
			.aspk_label{
				font-size:20px;
			}
			.aspk_value{
				font-size:20px;
				color:#1FA67A;
			}
			#send_message{
				margin-top:1em;
			}
			</style>
			<div class="tw-bs container" style="margin-top: 2em;">
				<div class="row">
					<div class="col-md-12" style="font-size:40px;margin-bottom:1em;" ><?php echo $single_ad->title;?></div>
				</div>
				<div class="row">
					<div class="col-md-5">
						<div class="row">
							<div class="col-md-12">
								<?php echo get_the_post_thumbnail($product_id,'large' );?>	
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<div class="row">
							<div class="col-md-4 aspk_label" style="margin-top:1em;">Game </div>
							<div class="col-md-8 aspk_value" style="margin-top:1em;"><?php echo $single_ad->catagory_slug;?></div>
						</div>
						
						<div class="row">
							<div class="col-md-4 aspk_label" style="margin-top:1em;">Price </div>
							<div class="col-md-8 aspk_value" style="margin-top:1em;"><?php echo $product->get_price_html().'g'; ?></div>
						</div>
						<div class="row">
							<div class="col-md-4 aspk_label" style="margin-top:1em;" >Catagory</div>
							<div class="col-md-8 aspk_value" style="margin-top:1em;" >
								<?php
									$cat = str_replace("-"," ",$single_ad->catagory_slug );
									echo ucwords($cat);
								?>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 aspk_label" style="margin-top:1em;" >Send Message to Seller</div>
						</div>
						<form method="post" action="">
							<div class="row">
								<input type="hidden" value="<?php echo $single_ad->adid; ?>" name="aspk_ad_id">
								<input type="hidden" value="<?php echo $uid; ?>" name="aspk_seler_name">
								<input type="hidden" value="<?php echo $b_uid; ?>" name="aspk_buyer_name">
								<input type="hidden" value="<?php echo $product_id; ?>" name="aspk_product_idd">
								<div class="col-md-12"  style="font-size:20px;margin-top:1em" ><textarea required name="aspk_buyer_message" rows="5" cols="10" style="height:6em;"></textarea></div>
							</div>
							<div class="row">
								<div class="col-md-12" style="font-size:20px;margin-top:1em" ><input type="submit" value="Send" class="btn btn-primary" name="aspk_submit_msg_list" /></div>
							</div>
						</form>
					</div>
				</div>
				<div class="row">
				<div class="col-md-6"><h2>Description<h2></div>
					<div class="col-md-10 aspk_label" ><?php echo $single_ad->description;?></div>
				</div>
			</div>
		<?php	
		}
		
		function show_statements($results){
			if(count($results) < 1){
			?>
			<div class="tw-bs container" style="min-height:30em"> <!-- start container -->
				<div class="row">
					<div class="col-md-12 digital_status" >
					Sorry, you have no account statements.
					</div>
				</div>
			</div><!-- end container -->
			<?php
			}else { 
			?>
			<div class="tw-bs container" style="min-height:30em"> <!-- start container -->
				<div class="row">
					<div class="col-md-12">
					</div>
				</div>
				<div class="row">
					<div class="aspk_ac_start col-md-2"><b>Date</b></div>
					<div class="aspk_acount col-md-2"><b>User Name</b></div>
					<div class="aspk_acount col-md-2" ><b> Credit</b></div>
					<div class="aspk_acount col-md-2" ><b>Debit</b></div>
					<div class="aspk_acount col-md-2" ><b> Balance</b></div>
					<div class="aspk_ac_end col-md-2"><b> Description</b></div>
				</div><?php 
					foreach($results as $stement){
						$uid = $stement->statement_user;
						$b_fname = get_user_meta( $uid, 'first_name',true);
						$b_lname = get_user_meta( $uid, 'last_name',true);
						$user_name = $b_fname.' '.$b_lname;
					  ?><div class="row">
							<div class="col-md-2 aspk_start" ><?php echo $stement->ac_date; ?></div>
							<div class="aspk_acount_st col-md-2"><?php echo $user_name; ?></div>
							<div class="aspk_acount_st col-md-2"><?php echo $stement->credit; ?></div>
							<div class="aspk_acount_st col-md-2"><?php echo $stement->debit; ?></div>
							<div class="aspk_acount_st col-md-2"><?php echo $stement->balance; ?></div>
							<div class="aspk_end col-md-2"><p><?php echo $stement->description; ?></p></div>
						</div>
						<?php
					} ?>
			</div><!-- end container -->
				<?php
			}
		}
		
	}// calss ends
}//if ends
