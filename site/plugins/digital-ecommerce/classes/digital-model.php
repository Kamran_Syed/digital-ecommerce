<?php

//Digital E-commerce Model
if(!class_exists('Aspk_Digital_Ecommerce_Model')){
	
	class Aspk_Digital_Ecommerce_Model{
		
		function Aspk_Digital_Ecommerce_Model(){
			
			
		}
		
		function get_want_list_products($user_id){
			global $wpdb;
			
			$sql="SELECT product_id FROM `".$wpdb->prefix."digital_ads` WHERE user_id='{$user_id}' AND is_want='1' AND status='publish'";
			return $wpdb->get_results($sql);
		}
		function all_want_list_ads($user_id){
			global $wpdb;
			$sql="SELECT * FROM `".$wpdb->prefix."digital_ads` WHERE user_id='{$user_id}' AND is_want='1'";
			return $wpdb->get_results($sql);
		}
		
		function all_have_list_ads($user_id){
			global $wpdb;
			$sql="SELECT * FROM `".$wpdb->prefix."digital_ads` WHERE user_id='{$user_id}' AND is_want='0' AND status='publish'";
			return $wpdb->get_results($sql);
		}
		
		function get_ads_list_for_user($user_id,$game,$end_limit){
			global $wpdb;
			
			$sql="SELECT * 
					FROM  `".$wpdb->prefix."digital_ads` 
					WHERE STATUS =  'publish'
					AND catagory_slug = {$game}
					AND is_want =0
					AND user_id != {$user_id}
					AND product_id
					IN (
					SELECT DISTINCT product_id
					FROM  `".$wpdb->prefix."digital_ads` 
					WHERE user_id = {$user_id}
					AND is_want =  '1'
					AND STATUS =  'publish' 
					)LIMIT 0,{$end_limit};
				";
			return $wpdb->get_results($sql);
		}
		function get_more_ads_list($user_id,$game,$st,$end){
			global $wpdb;
			$sql="SELECT * 
					FROM  `".$wpdb->prefix."digital_ads` 
					WHERE STATUS =  'publish'
					AND catagory_slug = {$game}
					AND is_want =0
					AND user_id != {$user_id}
					AND product_id
					IN (
					SELECT DISTINCT product_id
					FROM  `".$wpdb->prefix."digital_ads` 
					WHERE user_id = {$user_id}
					AND is_want =  '1'
					AND STATUS =  'publish' 
					)LIMIT {$st},{$end};
				";
			return $wpdb->get_results($sql);
		}
		
		function install_model(){
			global $wpdb;
			
			$sql="CREATE TABLE IF NOT EXISTS`".$wpdb->prefix."digital_ads` (
				`adid` int(11) NOT NULL AUTO_INCREMENT,
				`user_id` int(11) DEFAULT NULL,
				`title` text NOT NULL,
				`catagory_slug` varchar(255) NOT NULL,
				`product_id` int(11) NOT NULL,
				`description` longtext,
				`date_time` datetime NOT NULL,
				`deal_date` datetime DEFAULT NULL,
				`delivery_date` datetime DEFAULT NULL,
				`is_want` int(5) DEFAULT NULL,
				`status` varchar(25) NOT NULL,
				`deal_buyer_id` int(11) DEFAULT NULL, 
				`catagory_name` varchar(255) NOT NULL,
				`fb_buyer_dt` datetime DEFAULT NULL,
				`fb_seller_dt` datetime DEFAULT NULL,
				`trans_done` int(5) DEFAULT 0,
				`price` varchar(255) DEFAULT NULL,
				`seller_st` int(11) DEFAULT 0,
				`buyer_st` int(11) DEFAULT 0,
				PRIMARY KEY (`adid`)
			) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
			$wpdb->query($sql);
			
			$sql="CREATE TABLE IF NOT EXISTS`".$wpdb->prefix."digital_csv_upload` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `sku` varchar(255) NOT NULL,
			  `item_title` varchar(255) NOT NULL,
			  `price` varchar(255) NOT NULL,
			  `description` longtext NOT NULL,
			  `game_name` varchar(255) NOT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
			$wpdb->query($sql);
			
			$sql="CREATE TABLE IF NOT EXISTS`".$wpdb->prefix."digital_communication_msg` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `ad_id` int(11) NOT NULL,
			  `seller_uid` int(11)  NOT NULL,
			  `buyer_uid` int(11)  NOT NULL,
			  `msg_flow` longtext NOT NULL,
			  `date` datetime DEFAULT NULL,
			  `l_date` datetime DEFAULT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
			$wpdb->query($sql);
			
			$sql="CREATE TABLE IF NOT EXISTS`".$wpdb->prefix."digital_offers` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `of_ad_id` int(11) NOT NULL,
			  `buyer_userid` int(11)  NOT NULL,
			  `date` datetime DEFAULT NULL,
			  `ofr_price` varchar(255) NOT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
			$wpdb->query($sql);
			
			$sql="CREATE TABLE IF NOT EXISTS`".$wpdb->prefix."digital_account_statement` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `ac_ad_id` int(11) NOT NULL,
			  `ac_user_id` int(11)  NOT NULL,
			  `ac_buyer_id` int(11)  NOT NULL, 
			  `ac_date` datetime DEFAULT NULL,
			  `credit` varchar(255) NOT NULL,
			  `debit` varchar(255) NOT NULL,
			  `description` longtext ,
			  `balance` varchar(255) DEFAULT NULL,
			  `statement_user` int(11)  NOT NULL, 
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
			$wpdb->query($sql);
		}
		
		function get_all_statements($user){
			global $wpdb;
			
			$sql="SELECT * FROM `".$wpdb->prefix."digital_account_statement` WHERE statement_user ={$user}";
			return $wpdb->get_results($sql);
		}
		
		function insert_credit_to_account_st($ac_ad_id,$ac_user_id,$ac_buyer_id,$ac_date,$credit,$debit,$description,$balance,$statement_user){
			global $wpdb;
			
			$sql = "insert into `".$wpdb->prefix."digital_account_statement` ( ac_ad_id , ac_user_id,ac_buyer_id, ac_date,credit,debit,description,balance,statement_user) values('{$ac_ad_id}','{$ac_user_id}','{$ac_buyer_id}','{$ac_date}','{$credit}','{$debit}','{$description}','{$balance}','{$statement_user}')";
			$wpdb->query($sql);
		}
		
		function insert_credit_by_admin($ac_ad_id,$ac_user_id,$ac_buyer_id,$ac_date,$credit,$debit,$description,$balance,$statement_user){
			global $wpdb;
			
			$sql = "insert into `".$wpdb->prefix."digital_account_statement` ( ac_ad_id , ac_user_id,ac_buyer_id, ac_date,credit,debit,description,balance,statement_user) values('{$ac_ad_id}','{$ac_user_id}','{$ac_buyer_id}','{$ac_date}','{$credit}','{$debit}','{$description}','{$balance}','{$statement_user}')";
			$wpdb->query($sql);
		}
		
		function get_pending_seller_ofr($userid){
			global $wpdb;
			
			$sql="SELECT ad.*,ot.* FROM `".$wpdb->prefix."digital_ads` as ad, `".$wpdb->prefix."digital_offers` ot WHERE ad.adid =ot.of_ad_id AND ad.status = 'publish' AND ad.user_id = {$userid}";
			return $wpdb->get_results($sql);
		}
		
		function get_pending_buyer_ofr($userid){
			global $wpdb;
			
			$sql="SELECT ad.*,ot.* FROM `".$wpdb->prefix."digital_ads` as ad, `".$wpdb->prefix."digital_offers` ot WHERE ad.adid =ot.of_ad_id AND ad.status = 'publish' AND ot.buyer_userid = {$userid}";
			return $wpdb->get_results($sql);
		}
		
		function delete_offer($id){
			global $wpdb;
			
			$sql = "DELETE FROM `".$wpdb->prefix."digital_offers` WHERE id ='{$id}'";
			$wpdb->query($sql);
		}
		
		function get_active_seller_ofr($userid){
			global $wpdb;
			
			$sql="SELECT ad.*,ot.* FROM `".$wpdb->prefix."digital_ads` as ad, `".$wpdb->prefix."digital_offers` ot WHERE ad.adid =ot.of_ad_id AND ad.status != 'publish' AND ad.user_id = {$userid} AND ad.fb_seller_dt IS NULL ";
			return $wpdb->get_results($sql);
			
		}
		
		function get_active_buyer_ofr($userid){
			global $wpdb;
			
			$sql="SELECT ad.*,ot.* FROM `".$wpdb->prefix."digital_ads` as ad, `".$wpdb->prefix."digital_offers` ot WHERE ad.adid =ot.of_ad_id AND ad.status != 'publish' AND ot.buyer_userid = {$userid} AND fb_buyer_dt IS NULL";
			return $wpdb->get_results($sql);
			
		}
		
		function update_offer($ad_id,$buyerid,$price){
			global $wpdb;
			
			$sql = "UPDATE `".$wpdb->prefix."digital_offers` set  	
				ofr_price = '{$price}'			
				where of_ad_id ={$ad_id} AND buyer_userid = {$buyerid}";
			$wpdb->query($sql);
		}
		
		function offer_exists($adid, $buyerid){
			 global $wpdb;
			
			$sql="SELECT * FROM `".$wpdb->prefix."digital_offers` WHERE of_ad_id={$adid} AND buyer_userid ={$buyerid}";
			return $wpdb->get_row($sql);
		}
		
		function insert_offer($of_ad_id,$buyer_userid,$date,$ofr_price){
			global $wpdb;
			
			$sql = "insert into `".$wpdb->prefix."digital_offers` ( of_ad_id , buyer_userid, date,ofr_price) values('{$of_ad_id}','{$buyer_userid}','{$date}','{$ofr_price}')";
			$wpdb->query($sql);
		}
		
		function update_digital_active_trade_seller($ad_id,$val,$fb_seller_dt){
			global $wpdb;
			
			$sql = "UPDATE `".$wpdb->prefix."digital_ads` set  	
				status = 'Closed',
				fb_seller_dt = '{$fb_seller_dt}',
				trans_done = '{$val}',					
				seller_st = '1'					
				where adid ={$ad_id}";
			$wpdb->query($sql);
		}
		
		function update_digital_active_trade_buyer($ad_id,$val,$fb_buyer_dt){
			global $wpdb;
			
			$sql = "UPDATE `".$wpdb->prefix."digital_ads` set  	
				status = 'Closed',
				fb_buyer_dt = '{$fb_buyer_dt}',
				trans_done = '{$val}',					
				buyer_st = '1'					
				where adid ={$ad_id}";
			$wpdb->query($sql);
		}
		
		function update_digital_ad_close_deal_seller($ad_id,$val,$fb_seller_dt){
			global $wpdb;
			
			$sql = "UPDATE `".$wpdb->prefix."digital_ads` set  	
				status = 'Closed',
				fb_seller_dt = '{$fb_seller_dt}',
				trans_done = '{$val}',					
				seller_st = '1'									
				where adid ={$ad_id}";
			$wpdb->query($sql);
		}
		
		function update_digital_ad_close_deal_buyer($ad_id,$fb_buyer_dt){
			global $wpdb;
			
			$sql = "UPDATE `".$wpdb->prefix."digital_ads` set  	
				status = 'Closed',
				fb_buyer_dt = '{$fb_buyer_dt}'			
				where adid ={$ad_id}";
			$wpdb->query($sql);
		}
		
		function trans_done_by_ad_id($ad_id){
			global $wpdb;
			
			$sql="SELECT trans_done FROM `".$wpdb->prefix."digital_ads` WHERE adid ={$ad_id}";
			return $wpdb->get_var($sql);
		}
		
		function all_digital_ads_by_ad_id($ad_id){
			global $wpdb;
			
			$sql="SELECT * FROM `".$wpdb->prefix."digital_ads` WHERE adid ={$ad_id}";
			return $wpdb->get_row($sql);
		}
		
		function get_all_conversations($user_id){
			global $wpdb;
			
			$sql ="SELECT msg . * , ads . * 
					FROM `".$wpdb->prefix."digital_communication_msg` AS msg, `".$wpdb->prefix."digital_ads` AS ads
					WHERE (
					msg.`seller_uid` ={$user_id}
					OR msg.`buyer_uid` ={$user_id}
					)
					AND msg.ad_id = ads.adid
					AND ads.status = 'publish'
					";
			$rs = $wpdb->get_results($sql);
			return $rs;
		}
		
		function get_msg_flow($ad_id){
			global $wpdb;
			
			$sql = "SELECT msg_flow,buyer_uid FROM `".$wpdb->prefix."digital_communication_msg` WHERE `ad_id`='{$ad_id}'";
			$rs = $wpdb->get_row($sql);
			return $rs;
		}
		
		function insert_conf_into_d_com_msg($ad_id,$seller,$buyer,$msg_flow,$date,$l_date){
			global $wpdb;
			
			$sql = "insert into `".$wpdb->prefix."digital_communication_msg` ( ad_id , seller_uid, buyer_uid,msg_flow, date,l_date) values('{$ad_id}','{$seller}','{$buyer}','{$msg_flow}','{$date}','{$l_date}')";
			$wpdb->query($sql);
		}
		
		function update_conv_by_ad_id($ad_id,$msg,$l_date){
			global $wpdb;
			
			$sql = "UPDATE `".$wpdb->prefix."digital_communication_msg` set  
				msg_flow = '{$msg}'	,		
				l_date = '{$l_date}'	
				where ad_id ={$ad_id}";
			$wpdb->query($sql);
		}
		
		function update_dgt_ad_by_ad_id($d_ad_id,$d_buyer_userid,$date_time,$confirm,$price){
			global $wpdb;
			
			$sql = "UPDATE `".$wpdb->prefix."digital_ads` set  
				deal_buyer_id = {$d_buyer_userid},		
				status = '{$confirm}',
				deal_date = '{$date_time}',
				delivery_date = '{$date_time}',
				price = '{$price}'
				where adid ={$d_ad_id}";
			$wpdb->query($sql);
		}
		
		function insert_add_into_d_ad($user_id,$title,$catagory_slug,$product_id,$description,$date_time,$is_want,$status){
			global $wpdb;
			
			$sql = "insert into `".$wpdb->prefix."digital_ads` ( user_id , title, catagory_slug,product_id, description,date_time ,is_want ,status) values('{$user_id}','{$title}','{$catagory_slug}','{$product_id}','{$description}','{$date_time}','{$is_want}','{$status}')";
			$wpdb->query($sql);
		}
		
		function product_exist( $sku ) {
            global $wpdb;
            $product_id = $wpdb->get_var( $wpdb->prepare( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key='_sku' AND meta_value= %s LIMIT 1", $sku ) );
            return $product_id;
        }
		
		function insert_csv_digital($sku,$item_title,$price,$description,$game_name){
			global $wpdb;
			
			$sql = "insert into `".$wpdb->prefix."digital_csv_upload` ( sku , item_title, price, description, game_name ) values('{$sku}','{$item_title}','{$price}','{$description}','{$game_name}')";
			$wpdb->query($sql);
		}
		
		function get_all_sku_from_csv_digital(){
			global $wpdb;
			
			$sql = "SELECT sku FROM `".$wpdb->prefix."digital_csv_upload`";
			$rs = $wpdb->get_results($sql);
			return $rs;
		}
		
		function get_sku_from_csv_digital($sku){
			global $wpdb;
			
			$sql = "SELECT sku FROM `".$wpdb->prefix."digital_csv_upload` WHERE `sku` = '{$sku}'";
			$rs = $wpdb->get_results($sql);
			return $rs;
		}
		
		function get_detail_from_csv_digital($sku){
			global $wpdb;
			
			$sql = "SELECT * FROM `".$wpdb->prefix."digital_csv_upload` WHERE `sku` = '{$sku}'";
			$rs = $wpdb->get_row($sql);
			return $rs;
		}
		
		function all_digital_ads(){
			global $wpdb;
			$sql="SELECT * FROM `".$wpdb->prefix."digital_ads` WHERE 1";
			return $wpdb->get_results($sql);
		}
		
		function single_digital_ad($p_id){
			global $wpdb;
			$sql="SELECT * FROM `".$wpdb->prefix."digital_ads` WHERE product_id='{$p_id}'";
			return $wpdb->get_row($sql);
			
		}
		
		function empty_csv_table(){
			global $wpdb;
			
			$sql = "DELETE FROM `".$wpdb->prefix."digital_csv_upload`";
			$wpdb->query($sql);
		}
		
		function delete_ad_by_id($ad_id){
			global $wpdb;
			
			$sql = "DELETE FROM `".$wpdb->prefix."digital_ads` WHERE adid ='{$ad_id}'";
			$wpdb->query($sql);
		}
		function delete_msgs_by_id($ad_id){
			global $wpdb;
			
			$sql = "DELETE FROM `".$wpdb->prefix."digital_communication_msg` WHERE ad_id ='{$ad_id}'";
			$wpdb->query($sql);
		}
		
	}// class ends
	
}//if ends
