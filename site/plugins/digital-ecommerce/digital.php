<?php
/*
Plugin Name: Digital E-commerce
Plugin URI: 
Description: This plugins is essential to run Digital E-commerce Game. 
Author: Agile Solutions PK
Version: 1.5
Author URI: http://agilesolutionspk.com
*/
require_once(__DIR__ ."/classes/digital-model.php");

require_once(__DIR__ ."/classes/digital-view.php"); 

if(!(class_exists('Aspk_Digital_Ecommerce'))){
	class Aspk_Digital_Ecommerce{
		
		function Aspk_Digital_Ecommerce(){
			date_default_timezone_set('America/New_York');
			register_activation_hook( __FILE__, array(&$this, 'install') );
			add_shortcode('digital_result_view', array(&$this,'show_digital_list_view'));
			add_shortcode('aspk_account_statment', array(&$this,'show_digital_account'));
			add_shortcode('digital_user_dashboard', array(&$this,'show_digital_user_dashboard'));	
			add_shortcode('aspk_de_close_deal', array(&$this,'show_aspk_de_close_deal'));	
			add_action('admin_menu', array(&$this,'admin_menu'));
			add_action('wp_enqueue_scripts', array(&$this, 'wp_enqueue_scripts') );
			add_action('admin_enqueue_scripts', array(&$this, 'wp_enqueue_scripts') );
			add_action( 'wp_ajax_aspk_digital_game', array(&$this,'aspk_digital_game_call' ));
			add_action( 'wp_ajax_nopriv_aspk_digital_game', array(&$this,'aspk_digital_game_call' ));
			add_action( 'wp_ajax_aspk_digital_item', array(&$this,'aspk_digital_item_call' ));
			add_action( 'wp_ajax_nopriv_aspk_digital_item', array(&$this,'aspk_digital_item_call' ));
			add_action( 'wp_ajax_aspk_load_more', array(&$this,'aspk_digital_load_more' ));
			add_action( 'wp_ajax_nopriv_aspk_load_more', array(&$this,'aspk_digital_load_more' ));
			add_filter('manage_users_columns' , array(&$this,'add_balance_column'));
			add_filter('manage_users_custom_column', array(&$this, 'add_balance_column_value') , 99 , 3);
			//add_filter('woocommerce_currency_symbol',array(&$this,  'change_existing_currency_symbol'), 10, 2);
			add_action('wp_head',array(&$this,  'hook_javascript'));
			add_action( 'init', array(&$this, 'fe_init') );
			add_action( 'woocommerce_order_status_processing',array(&$this, 'order_status_changed')); 
		}
		
		function show_digital_account(){
			$dg_m = new Aspk_Digital_Ecommerce_Model();
			$dg_v = new Aspk_Digital_Ecommerce_View();
			
			$uid = get_current_user_id();
			$statements = $dg_m->get_all_statements($uid);
			$dg_v->show_statements($statements);
		}
		
		function order_status_changed( $order_id ) {
			global $woocommerce;
			
			$order = new WC_Order($order_id);
			$items = $order->get_items();
			
			$user_id = $order->user_id;
			if($user_id == 0) return;
			
			$user_balance = get_usermeta($user_id,'_aspk_de_bal');
			$user_amount = $user_balance['amount'];
			$user_desc = $user_balance['description'];
			$gold_balance = 0;
			foreach($items as $item){
				$pid = $item['product_id'];
				$goldvalue = get_post_meta( $pid, 'goldvalue',true );
				$gold_balance = $goldvalue + $gold_balance;
			}
			$total_amount = $user_amount + $gold_balance;
			
			$update_user_bal = array();
			$update_user_bal['amount'] = $total_amount;
			$update_user_bal['description'] = $user_desc;
			update_user_meta($user_id,'_aspk_de_bal',$update_user_bal);
			$message = $user_amount.' - '.$total_amount;
			$order->add_order_note($message);
			//$order->update_status('complete');
		}
		
		function fe_init(){
			if(! session_id()){
				$x = session_start();
			}
			ob_start();
		}
		
		function hook_javascript() {
			?>
			<script>
			jQuery(document).ready (function(){
				jQuery( ".entry-meta").hide();
				jQuery( ".nav-previous" ).hide();
				jQuery( ".nav-next" ).hide();
			});
			</script>;
			<?php
		}
		
		function change_existing_currency_symbol( $currency_symbol, $currency ) {
			switch( $currency ) {
				case '$': $currency_symbol = 'g'; break;
				case 'AUD': $currency_symbol = 'g'; break;
			} 
			$currency_symbol = 'g';
			return $currency_symbol;
		}
		
		function install(){
			$digital_model = new Aspk_Digital_Ecommerce_Model();
			$digital_model->install_model();
			
		}
		
		function admin_menu(){
			add_menu_page('Gold', 'Gold', 'manage_options', 'aspk_digital_gold', array(&$this, 'add_user_balance') );
			add_submenu_page( 'aspk_digital_gold','Add Balance', 'Add Balance', 'manage_options', 'aspk_add_balance', array(&$this, 'add_user_balance'));
			add_submenu_page( 'aspk_digital_gold','Bulk Import', 'Bulk Import', 'manage_options', 'bulk-import', array(&$this, 'bulk_import_product'));
		}
		
		function show_aspk_de_close_deal($attr){
			$dg_m = new Aspk_Digital_Ecommerce_Model();
			$dg_v = new Aspk_Digital_Ecommerce_View();
			if(isset($_POST['aspk_submit_close_d'])){
				$this->handle_close_deal_form();
			}
			$adid = $attr['adid'];
			$result = $dg_m->all_digital_ads_by_ad_id($adid);
			$fb_b_date = $result->fb_buyer_dt;
			$fb_s_date = $result->fb_seller_dt;
			if(empty($fb_b_date) || empty($fb_s_date )){
				$dg_v->show_close_trade($adid);
			}else{
				echo "<div class='row'><h2>You have already given feedback to this item.</h2></div>";
			}
		}
		
		function update_feedback_fr_seller($seller_uid,$f_b_val){
			
			$fb_sel_up = get_user_meta($seller_uid,'aspk_user_up_feedback',true);
			
			if($f_b_val == 1){
				$fb_sel_up = $fb_sel_up + 1;
				update_user_meta($seller_uid,'aspk_user_up_feedback',$fb_sel_up);
			}
			
			$fb_sel_down = get_user_meta($seller_uid,'aspk_user_down_feedback',true);
			if($f_b_val == 2){
				$fb_sel_down = $fb_sel_down + 1;
				update_user_meta($seller_uid,'aspk_user_down_feedback',$fb_sel_down);
			}

		}
		
		function update_feedback_fr_buyer($buyer_uid,$f_b_val){
			
			$fb_buyer_up = get_user_meta($buyer_uid,'aspk_user_up_feedback',true);
			
			if($f_b_val == 1){
				$fb_buyer_up = $fb_buyer_up + 1;
				update_user_meta($buyer_uid,'aspk_user_up_feedback',$fb_buyer_up);
			}
			
			$fb_buyer_d = get_user_meta($buyer_uid,'aspk_user_down_feedback',true);
			if($f_b_val == 2){
				$fb_buyer_d = $fb_buyer_d + 1;
				update_user_meta($buyer_uid,'aspk_user_down_feedback',$fb_buyer_d);
			}
		}
		
		function handle_close_deal_form(){
			$dg_m = new Aspk_Digital_Ecommerce_Model();
			
			$f_b_val = $_POST['aspk_fb_val'];
			$idd = $_POST['aspk_ad_id_trade'];
			$result = $dg_m->all_digital_ads_by_ad_id($idd);
			$uid = get_current_user_id();
			$title = $result->title;
			$seller_uid = $result->user_id;
			$buyer_uid = $result->deal_buyer_id;
			$price = $result->price;
			$transation = $dg_m->trans_done_by_ad_id($idd);
			$p_s = get_user_meta( $seller_uid,'_aspk_de_bal',true);
			$balance_sel = $p_s['amount'];
			$description_sel = $p_s['description'];
			$p_b = get_user_meta( $buyer_uid,'_aspk_de_bal',true);
			$balance_buyer = $p_b['amount'];
			$description_buyer = $p_s['description'];
			$date_time = date('Y-m-d H:i:s');
			
			if( $uid == $buyer_uid ){
				$this->update_feedback_fr_seller($seller_uid,$f_b_val);
			}
			if( $uid == $seller_uid ){
				$this->update_feedback_fr_buyer($buyer_uid,$f_b_val);
			}
			
			if(empty($transation)){
				echo "<div class='row'><h2>Thankyou for your Feedback.</h2></div>";
				
				$balance_buyer2 = $balance_buyer - $price;
				$balance_sel2 = $balance_sel + $price;
				
				$amount_meta_buyer=array();
				$amount_meta_buyer['amount'] = $balance_buyer2;
				$amount_meta_buyer['description'] = $description_buyer;
				update_user_meta( $buyer_uid,'_aspk_de_bal',$amount_meta_buyer);
				
				$amount_meta_seller=array();
				$amount_meta_seller['amount'] = $balance_sel2;
				$amount_meta_seller['description'] = $description_sel;
				update_user_meta( $seller_uid,'_aspk_de_bal',$amount_meta_seller);
				$tot_bal_se = get_user_meta( $seller_uid,'_aspk_de_bal',true);
				$total_sel_balance = $tot_bal_se['amount'];
				if( $uid == $buyer_uid ){
					$desc = $title.' -- Bought';
					$dg_m->insert_credit_to_account_st($idd,$seller_uid,$buyer_uid,$date_time,0,$price,$desc,$price,$buyer_uid);
				}
				if( $uid == $seller_uid ){
					$desc = $title.' -- Sold';
					$dg_m->insert_credit_to_account_st($idd,$seller_uid,$buyer_uid,$date_time,$price,0,$desc,$total_sel_balance,$seller_uid);
				}
				if( $uid == $seller_uid ){
					$dg_m->update_digital_active_trade_seller($idd,1,$date_time);
				}
				if( $uid == $buyer_uid ){
					$dg_m->update_digital_active_trade_buyer($idd,1,$date_time);
				}
			}else{
				if( $uid == $seller_uid ){
					$dg_m->update_digital_ad_close_deal_seller($idd,1,$date_time);
				}
				if( $uid == $buyer_uid ){
					$dg_m->update_digital_active_trade_buyer($idd,1,$date_time);
				}
				
			}
		}
		
		function insert_event($ad_id){
			$dg_m = new Aspk_Digital_Ecommerce_Model();
			
			$res = $dg_m->all_digital_ads_by_ad_id($ad_id);
			
			$user_id = $res->user_id;
			$date = $res->deal_date;
			$time =  explode(' ',$date);
			$time = $time[1];
			$fname = get_user_meta( $user_id, 'first_name',true);
			$lname = get_user_meta( $user_id, 'last_name',true);
			$full_name = $fname.' '.$lname;
			
			$b_user_id = $res->deal_buyer_id;
			
			$b_fname = get_user_meta( $b_user_id, 'first_name',true);
			$b_lname = get_user_meta( $b_user_id, 'last_name',true);
			$b_full_name = $b_fname.' '.$b_lname;
			
			$user_id_sel = 'u_'.$user_id ;
			$user_id_buy = 'u_'.$b_user_id ;
			$category_ids = array($user_id_sel,$user_id_buy ); 
			$pc = "<div class='row'><div class='col-md-12'><h2> $res->title</h2></div></div> ";
			$pc .= "<div class='row'><div class='col-md-12'><h2> $full_name  -- $b_full_name</h2></div></div>";
			//$pc .= "[aspk_de_close_deal adid='$ad_id']";
			
			$post = array(
				'post_author' => $user_id,
				'post_content' => $pc,
				'post_status' => "publish",
				'post_title' => $res->title,
				'post_parent' => '',
				'post_type' => "event-list-cal",
			);
			//Create post
			$post_id = wp_insert_post( $post );
			if($post_id){
				wp_set_object_terms( $post_id, $category_ids , 'category' );//buyer  u-56
				update_post_meta( $post_id, '_visibility', 'visible' );
				update_post_meta( $post_id, 'event-date', $date );
				update_post_meta( $post_id, 'event-time', $time );
				update_post_meta( $post_id, 'event-end', $date );
				
			 }
		}
		
		function add_balance_column($columns) {
			return array_merge( $columns, 
              array('user_balance' => __('Balance')) );
		}
		
		function add_balance_column_value($empty, $column_name, $user_id){
			if($column_name == 'user_balance'){
				$user_amount=get_usermeta($user_id,'_aspk_de_bal');
				$column_name= $user_amount['amount'];
				if(empty($column_name)){
				$column_name='0';
				}
				return $column_name;
			}
		}
		
	
		function aspk_digital_load_more(){
			$digital_model = new Aspk_Digital_Ecommerce_Model();
			if(isset($_POST)){
			$st = $_POST['aspk_end_limit'];
			$last = $st + 25; 
			$games= $_POST['aspk_game'];
			$user_id= $_POST['aspk_user_id'];
			$values="('".str_replace(",","' OR '",$games)."')";
			$want_list_ads=$digital_model->get_more_ads_list($user_id,$values,$st,25);
			$page_link = get_permalink();
			$uid = get_current_user_id();
		
				if($want_list_ads){
					ob_start();
					foreach($want_list_ads as $ad){
						$product_id=$ad->product_id;
						$product = get_product( $product_id );
						$user_data=get_userdata( $ad->user_id );
						$user_name=$user_data->user_nicename;
						$up=get_user_meta($ad->user_id,'aspk_user_up_feedback');
						if(empty($up)) { $up = 0;}
						$down=get_user_meta($ad->user_id,'aspk_user_down_feedback');
						if(empty($down)) { $down = 0;}
					?>
					<div class="row">
						<div class="col-md-2" style="margin-top:1em;">
							<?php echo get_the_post_thumbnail($product_id,'thumbnail');?>	
						</div>
						<div class="col-md-1" style="margin-top:1em"><?php echo $user_name; ?> </div>
						<div class="col-md-1" style="margin-top:1em">
							<div class="row">
									<div class="col-md-6"><img style="width:30px" src="<?php echo plugins_url('img/ThumbsUp',__FILE__); ?>"><?php echo '('.$up.')' ;?>
									</div>
									<div class="col-md-6"><img style="width:30px" src="<?php echo plugins_url('img/ThumbsDown',__FILE__); ?>" ><?php echo '('.$down.')' ;?>
									</div>
							</div>
						</div>
						<div class="col-md-4">
							<form method="post" action="">
								<div id="aspk_show_ofr_<?php echo $ad->adid; ?>" style="display:none;background-color:#A29393;padding:1em;border-radius:10px;">
									<div class="row">
										<div class="col-md-12" >
											<div style="float:left;"><b>Offer Price</b></div>
											<div style="float:left;margin-left:1em;">
												<input type="text" required value="" name="agile_price">
												<input type="hidden" value="<?php echo $ad->adid; ?>" name="aspk_ad_id_ofr">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-8" style="margin-top:1em;">
											<input type="submit" class="btn btn-primary" name="agile_submit_ofer" value="Make Offer">
										</div>
										<div class="col-md-4" style="margin-top:1em;">
											<input type="button"  value="No Thanks" onclick="hide_offer_<?php echo $ad->adid; ?>();" class="btn btn-danger">
										</div>
									</div>
								</div>
							</form>
							<div class="row">
								<div class="col-md-12" style="font-size:40px;" ><a href="<?php echo $page_link.'?pid='.$product_id.'/&act='.'detail_page';?>"><?php echo $product->post->post_title; ?></a>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12"  ><p><?php echo $product->post->post_content;?></p></div>
							</div>
							<div class="row">
								<div class="col-md-12" style="font-size:20px;" ><?php echo $product->regular_price; ?></div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="row">
								<div class="col-md-12">
									<input type="button" class="btn btn-primary" id="aspk_btn_ofr_<?php echo $ad->adid; ?>" value="Make Offer" onclick="show_offer_div_<?php echo $ad->adid; ?>();">
								</div>
							</div>
						</div>
					</div>
					<script>
							function show_offer_div_<?php echo $ad->adid; ?>(){
								jQuery("#aspk_btn_ofr_<?php echo $ad->adid; ?>").hide();
								jQuery("#aspk_show_ofr_<?php echo $ad->adid; ?>").show();
							}
							
							function hide_offer_<?php echo $ad->adid; ?>(){
								jQuery("#aspk_btn_ofr_<?php echo $ad->adid; ?>").show();
								jQuery("#aspk_show_ofr_<?php echo $ad->adid; ?>").hide();
							}
					</script>
				<?php
					}
				$html=ob_get_clean();
					$ret = array('act'=>'show_ad','st'=>'ok','msg'=>'show_result','html_result'=>$html, 'last_id'=>$last);
					echo json_encode($ret);
					exit;	
				}
			}
		}
		
		function add_user_balance(){
			$dg_model = new Aspk_Digital_Ecommerce_Model();
			if(isset($_POST['submit_balance'])){
				$user_id= $_POST['aspk_user'];
				$user_info = get_userdata($user_id);
				$username=$user_info->user_nicename;
				$new_user_amount= $_POST['aspk_user_amount'];
				$amount_description= $_POST['aspk_amount_description'];
				$p_balance=get_user_meta($user_id, '_aspk_de_bal', true);
				$p_amount = $p_balance['amount'];
				$user_amount = $new_user_amount + $p_amount;
				$int_check= is_numeric($user_amount);
				$ac_user_id = get_current_user_id();
				$b_fname = get_user_meta( $ac_user_id, 'first_name',true);
				$b_lname = get_user_meta( $ac_user_id, 'last_name',true);
				$user_name = $b_fname.' '.$b_lname ;
				$ac_date = date('Y-m-d H:i:s');
			
				if($int_check==true){
					$amount_meta=array();
					$amount_meta['amount']= $user_amount;
					$amount_meta['description']= $amount_description;
					update_user_meta( $user_id,'_aspk_de_bal',$amount_meta);
					if($new_user_amount < 0){
						$debit = $new_user_amount * -1;
						$description = 'gold subtracted by '.$user_name;
						$dg_model->insert_credit_by_admin(0,$ac_user_id,$user_id,$ac_date,0,$debit,$description,$debit,$user_id);
					}else{
						$t_balance=get_user_meta($user_id, '_aspk_de_bal', true);
						$total_amount = $t_balance['amount'];
						$credit = $new_user_amount;
						$description = 'gold added by '.$user_name;
						$dg_model->insert_credit_by_admin(0,$user_id,$user_id,$ac_date,$credit,0,$description,$total_amount,$user_id);
					}
					
					echo "<h2 style='color:green;'>".$new_user_amount.' has been added to '.$username."</h2>";
				}
				else{
					echo "<h2 style='color:red;'>Please enter valid amount</h2>";
				}
			}
			?>
			<div class="tw-bs container" style="float:left;margin-top: 2em;">
				<form method="post" action="">
					<div class="row">
						<div class="col-md-3" style="font-size:20px;margin-top:1em;">
						User Name
						</div>
						<div class="col-md-9" style="margin-top:1em;">
							<select class="form-control" name="aspk_user" required/>
							<option value="">select user</option >
							<?php
							$all_users=get_users();
							foreach($all_users as $user){
								$user_id=$user->data->ID;
								$user_name=$user->data->user_nicename;
								?>
								<option value="<?php echo $user_id; ?>"><?php echo $user_name; ?></option >
						<?php   }  ?>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3" style="font-size:20px;margin-top:1em;" >
						Amount
						</div>
						<div class="col-md-9" style="font-size:20px;margin-top:1em;" >
							<input class="form-control" type="text" name="aspk_user_amount" required/>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3" style="font-size:20px;margin-top:1em;" >
						Description
						</div>
						<div class="col-md-9" style="font-size:20px;margin-top:1em;" >
						<textarea name="aspk_amount_description" class="form-control" rows="5" cols="5" style="width:30em;height:8em;" required/></textarea>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12" style="font-size:20px;margin-top:1em;" >
						<input class="btn btn-primary" type="submit" name="submit_balance">
						</div>
					</div>
				</form>	
			</div>
		<?
		}
		function bulk_import_product(){
			$this->handle_csv_upload();
			$step = '1';
			if(isset($_GET['step'])){
				$step = $_GET['step'];
			}
			?>
			<div class="tw-bs container">
				<div class="row">
					<div class="col-md-12">
						<h2 style="text-align:center;">Upload Products and their Images</h2>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12" style="margin-bottom:1em;">
						<h2>Step 1</h2>
					</div>
				</div>
				<div style="clear:left;background-color:white;padding:1em;float:left;border:1px solid;">
					<form method="post" name="upload_excel" enctype="multipart/form-data" action="">
						<div class="row">
							<div class="col-md-12" style="margin-top:1.3em;">
								<div style="float:left;"><b>Upload Products Detail</b></div>
								<div style="float:left;margin-left:1.2em;"><input required type="file" name="file" id="file" class="input-large"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<input type="submit" id="submit" value="Upload" name="Import" class="btn btn-primary button-loading" >		
							</div>
						</div>
					</form>
				</div>
				<div class="row">
					<div class="col-md-12"  style="margin-top:2.5em;margin-bottom:1em;">
						<h2>Step 2</h2>
					</div>
				</div>
				<div style="clear:left;background-color:white;padding:1em;float:left;border:1px solid;">
					<div class="row">
						<div class="col-md-12" >
							<form method="post" name="upload_excel" enctype="multipart/form-data" action="">
								<div class="row">
									<div class="col-md-12" style="margin-top:1.3em;">
										<div style="float:left;"><b>Upload  Product Images</b></div>
										<div style="float:left;margin-left:1.2em;"><input required type="file" name="file_pic" id="file" class="input-large"></div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<input type="submit" id="submit" value="Upload Images" name="Import_pic" class="btn btn-primary button-loading" >		
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12" style="text-align:center;"><h1 >OR</h1></div>
					</div>
					<div class="row">
						<div class="col-md-12" >
							<form method="post" action="">
								<div class="row">
									<div class="col-md-12" style="margin-top:1.3em;">
										<div style="float:left;"><b>wp-content/uploads/</b></div>
										<div style="float:left;margin-left:1.2em;"><input type="text" required name="get_images_folder" value="" ></div>	
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<input type="submit" name="get_images_ftp" value="Process" class="btn btn-primary">
									</div>	
								</div>	
							</form>
						</div>
					</div>
				</div>
			</div>
			<?php
		}
		
		function handle_csv_upload(){
			$d_model = new Aspk_Digital_Ecommerce_Model();
			
			if(isset($_POST["Import"])){
				$d_model->empty_csv_table();
				if ( ! function_exists( 'wp_handle_upload' ) );
				require_once( ABSPATH . 'wp-admin/includes/file.php' );
				$uploadedfile = $_FILES['file'];
				$upload_overrides = array( 'test_form' => false );
				$movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
				if ($movefile){
					$file_path = $movefile['file'];
					$handle = fopen($file_path, "r");
					$row = 0;
					while (($data = fgetcsv($handle, 10000, "\t")) !== FALSE){
						$row++;
						$num = count($data);
						if($row == 1) continue;
						if($num < 2) continue; //blank lines with EOL chracter
						
						if($num != 5){
							echo "<p> Incorrect format of uploaded file: $num fields in line $row: <br /></p>\n";
							//break;
						}
						$sku = $data[0];
						$item_title =  $data[1];
						$price = $data[2];
						$description = $data[3];
						$game_name = $data[4];
				
						$d_model->insert_csv_digital($sku, $item_title, $price, $description,$game_name);
						
					}	
					echo "<h2>File Imported Successfully</h2>";
					fclose( $handle );
				}
			}	
			if(isset($_POST["Import_pic"])){
				if ( ! function_exists( 'wp_handle_upload' ) )
					require_once( ABSPATH . 'wp-admin/includes/file.php' );
				$uploadedfile = $_FILES['file_pic'];
				$upload_overrides = array( 'test_form' => false );
				$path_parts = pathinfo($_FILES["file_pic"]["name"]);
				$file_name = $path_parts['filename'];
				$movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
				if ($movefile){
					$zip = new ZipArchive;
					$file_path = $movefile['file'];
					
					$path = pathinfo(realpath($file_path), PATHINFO_DIRNAME);
					
					$res = $zip->open($file_path);
					if ($res === TRUE) {
						$zip->extractTo( $path);
						$zip->close();
						$this->process_bulk_import($path);
					}else{
						echo "<div class='error'>Unzip Failed.</div>";
					}
						
						echo '<h2>Images are uploaded successfully.</h2>';
				} else {
					  echo "<div class='error'>Uploading Failed.</div>";
				}
			}
			if(isset($_POST['get_images_ftp'])){
				$folder = $_POST['get_images_folder'];
				$dir = ABSPATH."wp-content/uploads/".$folder;
				if(! file_exists ( $dir )){
					echo "<div class='error'>Sorry, I cannot find folder ".$dir."</div>";
					return;
				}
				$this->process_bulk_import($dir);
			}
		}
		
		function process_bulk_import($real_path){
			$d_model = new Aspk_Digital_Ecommerce_Model();
			
			$all_sku =$d_model->get_all_sku_from_csv_digital();
			
			if(count($all_sku) < 1){
				//show error and return TODO
				echo "<div class='error'>Sorry, sku not available</div>";
			}
			foreach($all_sku as $s){
				$img1 = "img-".$s->sku .'.jpg';
				$img2 = "img-".$s->sku .'.png';
				
				$img_full_path = $real_path.'/'.$img1;
				$img_full_path2 = $real_path.'/'.$img2;
				$product = $d_model->product_exist( $s->sku );
				if(empty($product)){
					$notfound = true;
					if(is_file($img_full_path)){
						$notfound = false;
						$results = $d_model->get_detail_from_csv_digital($s->sku);
						$this->insert_product_detail_img($results,$img_full_path);
						?>
						<h2><?php echo 'Product successfully created with title '.$results->item_title; ?> </h2>
						<?php
					}elseif(is_file($img_full_path2)){
						$notfound = false;
						$results = $d_model->get_detail_from_csv_digital($s->sku);
						$this->insert_product_detail_img($results,$img_full_path2);
						?>
					<h2><?php echo 'Product successfully created with title '.$results->item_title; ?> </h2>
					<?php
					}
					if($notfound){
						//Image for $s->sku not found in $img_full_path
						echo "<div class='error'>Image for ".$s->sku ." not found in ".$img_full_path2."</div>";
					}
				}else{
					?>
					<h2><?php echo 'Product already exist with '.$s->sku; ?> </h2>
					<?php
				}
			}
		}
		function insert_product_detail_img($results,$img_full_path){
			if($results){
				$dgt_sku = $results->sku;
				$dgt_title = $results->item_title;
				$dgt_price = $results->price;
				$dgt_desc = $results->description;
				$dgt_game_name = $results->game_name;
				$user_id = get_current_user_id();
				$post = array(
					'post_author' => $user_id,
					'post_content' => $dgt_desc,
					'post_status' => "publish",
					'post_title' => $dgt_title,
					'post_parent' => '',
					'post_type' => "product",
				);
				//Create post
				$post_id = wp_insert_post( $post );
				if($post_id){
					wp_set_object_terms( $post_id, $dgt_game_name , 'product_cat' );
					update_post_meta($post_id, '_sku', $dgt_sku);
					update_post_meta( $post_id, '_price', $dgt_price );
					update_post_meta( $post_id, '_regular_price', $dgt_price );
					update_post_meta( $post_id, '_visibility', 'visible' );
					$wp_filetype = wp_check_filetype($img_full_path, null );
					$attachment = array(
						'post_mime_type' => $wp_filetype['type'],
						'post_title' =>  $dgt_title,
						'post_content' => $dgt_desc,
						'post_status' => 'inherit'
					);

					$attach_id = wp_insert_attachment( $attachment, $img_full_path, $post_id );
					require_once(ABSPATH . 'wp-admin/includes/image.php');
					$attach_data = wp_generate_attachment_metadata( $attach_id, $img_full_path );
					wp_update_attachment_metadata( $attach_id, $attach_data );

					set_post_thumbnail( $post_id, $attach_id );
				}
				
				
			}
		}
		
		function wp_enqueue_scripts(){
			wp_enqueue_script('jquery');
			wp_enqueue_script('js-fe-bootstrap',plugins_url('js/js-agile-bootstrap.js', __FILE__)
			);
			wp_enqueue_style( 'agile-fe-bootstrap', plugins_url('css/agile-bootstrap.css', __FILE__) );
			wp_enqueue_style( 'agile-fe-digital', plugins_url('css/digital.css', __FILE__) );
			wp_enqueue_style( 'css-datepicker', plugins_url('css/datepicker.css', __FILE__) );
			
			wp_enqueue_script('js-calendar',plugins_url('js/calendar_js.js', __FILE__)
			);
			wp_enqueue_script('js-csv-bootstrap',plugins_url('js/datepicker.js', __FILE__)
			);
		}
		
		function aspk_digital_item_call(){
			$digital_model = new Aspk_Digital_Ecommerce_Model();
			$all_ads=$digital_model->all_digital_ads();
			$dropdown_view = new Aspk_Digital_Ecommerce_View();
			$product_id = $_POST['digital_item'];
				$product = get_product( $product_id );
				$pid=$product->post->ID;
				$product_title=$product->post->post_title;
				$price=$product->regular_price;
				ob_start();
				?>
				<div class="row">
					<div class="col-md-8" >
						<div class="row">
							<div class="col-md-4" style="margin-top:1em">Description</div>
							<div class="col-md-8" style="margin-top:1em">
							<textarea class="form-control" name="aspk_description" rows="5" cols="3" style="height:10em"><?php echo $product->post->post_content;?></textarea></div>
						</div>
						<div class="row">
							<div class="col-md-4" style="margin-top:1em">Asking Price</div>
							<div class="col-md-8" style="margin-top:1em;">
									<input type="text" name="aspk_price" class="form-control" value="<?php echo $product->regular_price;?>"/>
									<input type="hidden" name="aspk_prod_id" value="<?php echo $product_id; ?>">	
									<input type="hidden" name="aspk_prod_title" value="<?php echo $product_title; ?>">	
							</div>
						</div>
						<div class="row">
							<div class="col-md-4" style="margin-top:0.5 ">Tags</div>
							<div class="col-md-8" style="border: 1px solid #cccccc; width: 27.5em;margin-left: 1em;padding: 0.5em;min-height:2em;border-radius: 3px">
							<?php echo $product->get_tags();?>	
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-4" style="margin-top:1em;">List</div>
							<div class="col-md-4" style="">
								<input type="submit" name="submit_want" value="Add to Want List" style="color:white;margin-top:1em;"/>
							</div>
							<div class="col-md-4" style="">
								<input type="submit" name="submit_have" value="Add to Have List" style="color:white;margin-top:1em;"/>
							</div>
						</div>	
					</div>	
					<div class="col-md-4" >
						<div  style="" id="aspk_feature_image10">
							<?php echo get_the_post_thumbnail($product_id,'thumbnail');?>	
						</div>
					</div>
				</div>
				<script>
					jQuery(document).ready (function(){
						jQuery("#aspk_item121 a").attr('target', '_blank');
							jQuery("#aspk_tags").val(jQuery("#item_tags a").attr ('href'));
								 jQuery("#aspk_img_src").val(jQuery("#aspk_feature_image img").attr ('src'));
					});
				</script>
				<?php
				$html =  ob_get_clean();
				$ret = array('act' => 'show_model','st'=>'ok','msg'=>'show results', 'html_reuslt'=>$html );
				echo json_encode($ret);
				exit;
		}
		
		function aspk_digital_game_call(){
			$dropdown_view = new Aspk_Digital_Ecommerce_View();
			$game = $_POST['aspk_game'];
			$item_id = $dropdown_view->get_items($game);
			if($item_id){
			ob_start();
			
			?><option value="">Select Item</option><?php
				foreach ($item_id as $id){
				$product = get_product( $id );
				$pid=$product->post->ID;
				$product_title=$product->post->post_title;
				?>
				<option value="<?php echo $pid;?>"><?php echo $product_title;?></option><?php
				}
			?>
			<?php
			}else{
				?><div >None</div><?php
			}
				$html =  ob_get_clean();
				$ret = array('act' => 'show_model','st'=>'ok','msg'=>'show results', 'html_reuslt'=>$html );
				echo json_encode($ret);
				exit;
		}
		
		function show_digital_user_dashboard(){
			$digital_dash_view = new Aspk_Digital_Ecommerce_View();
			$digital_model = new Aspk_Digital_Ecommerce_Model();
			$user_id = get_current_user_id();
			$b_fname = get_user_meta( $user_id, 'first_name',true);
			$b_lname = get_user_meta( $user_id, 'last_name',true);
			$full_name = $b_fname.' '.$b_lname;
			$date_time = date('Y-m-d H:i:s');
			if($user_id > 0 ){
			$digital_dash_view->aspk_show_user_dashboard();
				if(!isset($_GET['aspk_page'])){
				$this->aspk_show_inbox();
				}
			}
			else{
			?>
			<div class="row">
				<div class="col-md-12" style="font-size:20px">Please <a href="<?php echo wp_login_url(get_permalink());?>" title="Login">login</a> to access Member Dashboard
				</div>
			</div>
			<?php
			}
			
			if(isset($_POST['submit_want'])){
				$catagory_slug = $_POST['aspk_game'];
				$title = $_POST['aspk_prod_title'];
				$description = $_POST['aspk_description'];
				$product_id = $_POST['aspk_prod_id'];
				$product_price = $_POST['aspk_price'];
				update_post_meta($product_id,'_regular_price',$product_price);
				
				$args = array( 
					'ID'           => $product_id,
					'post_content' => $description
				);
				wp_update_post($args);
				$digital_model->insert_add_into_d_ad($user_id,$title,$catagory_slug,$product_id,$description,$date_time,1,'publish');
				?>
				<div class="tw-bs container">
					<div class="row">
						<div class="col-md-12 digital_status">
							<?php echo $title.' has been added to want list.'?>
						</div>
					</div>
				</div>
				<?php
			}
			if(isset($_POST['submit_have'])){
				$catagory_slug = $_POST['aspk_game'];
				$title = $_POST['aspk_prod_title'];
				$description = $_POST['aspk_description'];
				$product_id = $_POST['aspk_prod_id'];
				$product_price = $_POST['aspk_price'];
				
				update_post_meta($product_id,'_regular_price',$product_price);
				$args = array( 
					'ID'           => $product_id,
					'post_content' => $description
				);
				wp_update_post($args);
				$digital_model->insert_add_into_d_ad($user_id,$title,$catagory_slug,$product_id,$description,$date_time,0,'publish');
				?>
				<div class="tw-bs container">
					<div class="row">
							<div class="col-md-12 digital_status">
								<?php echo $title.' has been added to have list.'?>
							</div>
					</div>
				</div>
				<?php
			}
			if(isset($_GET['aspk_page'])){
				$page=$_GET['aspk_page'];
				if($page == 'add_item'){
					$digital_dash_view->show_add_item();
				}
				if($page == 'want_list'){
					$p_id= $_GET['pid'];
					if(isset($_GET['act'])){
						if($_GET['act'] == 'detail_page'){
							$single_ad=$digital_model->single_digital_ad($p_id);
							$digital_dash_view->aspk_show_detail_view_to_current_user($single_ad);
							}
					}
					else{
						$ad_id = $_GET['aspk_del_ad'];
						if($ad_id){
							$digital_model->delete_ad_by_id($ad_id);
						}
						$want_list_ads=$digital_model->all_want_list_ads($user_id);
						$user_balance=get_usermeta($user_id,'_aspk_de_bal');
						$user_amount= $user_balance['amount'];
						if(!$want_list_ads){ ?>
							<div class="tw-bs container" style="min-height:30em">
								<div class="row">
									<div class="col-md-12" style="float:right;font-size:20px;margin-top:1em;">
										Current Balance: <span style="color:green;font-weight: bold;"><?php echo $user_amount.'g';?></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12 digital_status">
									Currently there are no items in your Want list. Please check back later
									</div>
								</div>
							</div>
				<?php	}	
						$digital_dash_view->product_list_show($want_list_ads);
					}
				}
				if($page == 'have_list'){
					$p_id= $_GET['pid'];
					if(isset($_GET['act'])){
						if($_GET['act'] == 'detail_page'){
							$single_ad=$digital_model->single_digital_ad($p_id);
							$digital_dash_view->aspk_show_detail_view_to_current_user($single_ad);
						}
					}
					else{
						$ad_id = $_GET['aspk_del_ad'];
						if($ad_id){
							$digital_model->delete_ad_by_id($ad_id);
						}
						$have_list_ads=$digital_model->all_have_list_ads($user_id);
						$user_balance=get_usermeta($user_id,'_aspk_de_bal');
						$user_amount= $user_balance['amount'];
						if(!$have_list_ads){ ?>
							<div class="tw-bs container" style="min-height:30em">
								<div class="row">
									<div class="col-md-12" style="float:right;font-size:20px;margin-top:1em;">
										Current Balance: <span style="color:green;font-weight: bold;"><?php echo $user_amount.'g';?></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12 digital_status">
									Currently there are no items in your Have list. Please check back later
									</div>
								</div>
							</div>
				<?php	}	
						$digital_dash_view->product_list_show($have_list_ads);
					}
				}
				if($page == 'pending_trade'){
					if(isset($_POST['aspk_submit_msgrr21'])){
						$this->handle_comunication_form();
					}
					if(isset($_GET['aspk_del_ad'])){
						$id = $_GET['aspk_del_ad'];
						$digital_model->delete_offer($id);
					}
					if(isset($_POST['aspk_accept_offer'])){
						$adid = $_POST['agile_ad_id'];
						$buyerid = $_POST['agile_buyer_id'];
						$selerid = $_POST['agile_seler_id'];
						$price = $_POST['agile_price'];
						$date = $_POST['aspk_date'];
						$hour = $_POST['aspk_hr'];
						$mint = $_POST['aspk_time'];
						$year = explode('/',$date);
						$month = $year[0];
						$day = $year[1];
						$yr =  end($year);
						$ful_date = $yr.'-'.$month.'-'.$day.' '.$hour.':'.$mint.':00';
						$digital_model->update_dgt_ad_by_ad_id($adid,$buyerid,$ful_date,'Confirm',$price);
						$this->insert_event($adid);
					}
					$offers_seller =$digital_model->get_pending_seller_ofr($user_id);
					if(count($offers_seller) < 1){
					}else{
						$digital_dash_view->show_record_items($offers_seller);
					}
					$offers_buyer =$digital_model->get_pending_buyer_ofr($user_id);
					if(count($offers_buyer) < 1){
					}else{
						$digital_dash_view->show_record_items($offers_buyer);
					}
					if(empty($offers_seller) && empty($offers_buyer)){
					?>
					<div class="tw-bs container" style="min-height:30em">
						<div class="row">
							<div class="col-md-12 digital_status" >
								There are no Pending Trades
							</div>
						</div>
					</div>
					<?php
					}
				}
				if($page == 'active_trade'){
					if(isset($_POST['aspk_submit_close_d'])){
						$idd = $_POST['aspk_ad_id_trade'];
						$this->handle_close_deal_form();
					}
					$offers_seller_active =$digital_model->get_active_seller_ofr($user_id);
					if(count($offers_seller_active) < 1){
					}else{
						$digital_dash_view->show_active_trades($offers_seller_active);
					}
					$offers_buyer_active =$digital_model->get_active_buyer_ofr($user_id);
					if(count($offers_buyer_active) < 1){
					}else{
						$digital_dash_view->show_active_trades($offers_buyer_active);
					}
					if(empty($offers_seller_active) && empty($offers_buyer_active)){
					?>
					<div class="tw-bs container" style="min-height:30em">
						<div class="row">
							<div class="col-md-12 digital_status">
								There are no Active Trades
							</div>
						</div>
					</div>
					<?php
					}
				}
				if($page == 'digital_inbox'){
					$this->aspk_show_inbox();
				}
				if($page == 'digital_calender'){
					
					echo do_shortcode('[mini-calendar]');
				}
			}
		}
		
		function aspk_show_inbox(){
			$digital_model = new Aspk_Digital_Ecommerce_Model();
			$digital_dash_view = new Aspk_Digital_Ecommerce_View();
			$date_time = date('Y-m-d H:i:s');
			$user_id = get_current_user_id();
			$b_fname = get_user_meta( $user_id, 'first_name',true);
			$b_lname = get_user_meta( $user_id, 'last_name',true);
			$full_name = $b_fname.' '.$b_lname;
			$ad_id = $_GET['aspk_del_msgs'];
			if($ad_id){
				$digital_model->delete_msgs_by_id($ad_id);
			}
			if(isset($_POST['aspk_submit_deal'])){
				$d_buyer_userid = $_POST['aspk_buyer_id_d'];
				$d_ad_id = $_POST['aspk_ad_id_d'];
				$d_price = $_POST['aspk_price_d'];
				$d_game = $_POST['aspk_game_d'];
				$digital_model->update_dgt_ad_by_ad_id($d_ad_id,$d_buyer_userid,$date_time,$date_time,'Confirm',$d_price);
				$this->insert_event($d_ad_id);
			}
			if(isset($_POST['aspk_reply_msg'])){
				$msg = $_POST['aspk_reply_conv'];
				$ad_idd = $_POST['aspk_ad_id_bila'];
				$text = $digital_model->get_msg_flow($ad_idd);
				if($text->buyer_uid == $user_id){
					$color = '#FDE7E7';
				}else{
					$color ='#F5CC99';
				}
				ob_start();
				?>
				<div class="row">
					<div class="col-md-12">
							<p><?php echo $text->msg_flow;?></p>
					</div>
				</div>
				<div style="margin-top:1em;clear:left;padding:1em;border-radius:10px;background-color:<?php echo $color; ?>;">
					<div class="row">
						<div class="col-md-12">
							<b><?php echo $full_name.' '.$date_time;?></b>
							<p><?php echo $msg;?></p>
						</div>
					</div>
				</div>
				<?php
				$html = ob_get_clean(); 
				$digital_model->update_conv_by_ad_id($ad_idd,$html,$date_time);
			}
				$result = $digital_model->get_all_conversations($user_id);
				$digital_dash_view->show_inbox_screen($result);
		}

		function show_digital_list_view($atts){
			$args = array(
				'number'     => '',
				'orderby'    => 'name',
				'order'      => 'ASC',
				'hide_empty' => 1
			);
			$product_categories = get_terms( 'product_cat', $args );
			$all_cat = array();
			foreach($product_categories as $cat){
				$all_cat[] = $cat->slug;
			}
			$x = implode(',',$all_cat);
			 $game_attr = shortcode_atts( array(
			'games' => $x
			), $atts );
			$game_values="'".$game_attr['games']."'";
			$values='('.str_replace(",","' OR '",$game_values).')';
			$digital_view = new Aspk_Digital_Ecommerce_View();
			$digital_model = new Aspk_Digital_Ecommerce_Model();
			$user_id = get_current_user_id();
			$date_time = date('Y-m-d H:i:s');
			if($user_id == 0){
				?>
				
				<div class='error'>Please <a href="<?php echo wp_login_url(get_permalink());?>" title="Login">login</a> to view Items</div>
				<?php
				return ;
			}
			
			if(isset($_POST['agile_submit_ofer'])){
				$price = $_POST['agile_price'];
				$ad_idd = $_POST['aspk_ad_id_ofr'];
				if(is_numeric($price)){
					$exist_ofr = $digital_model->offer_exists($ad_idd,$user_id);
					if(count($exist_ofr) < 1){
						$digital_model->insert_offer($ad_idd,$user_id,$date_time,$price);
						?>
						<div class="tw-bs container">
							<div class="row">
								<div class="col-md-12 digital_status" >
									Your Offer <?php echo $price;  ?>has been sent.
								</div>
							</div>
						</div>
						<?php
					}else{
						$digital_model->update_offer($ad_idd,$user_id,$price);
						?>
						<div class="tw-bs container">
							<div class="row">
								<div class="col-md-12 digital_status" >
									Your Offer <?php echo $price;  ?>has been updated.
								</div>
							</div>
						</div>
						<?php
					}
				}else{ ?>
						<div class="tw-bs container">
							<div class="row">
								<div class="col-md-12 digital_status" >
									Please enter valid offered Price.
								</div>
							</div>
						</div>
		<?php	}
			}
			if(isset($_POST['aspk_submit_msg'])){
				$this->handle_comunication_form();
			}
			if(isset($_GET['act'])){
				$p_id = $_GET['pid'];
				if($_GET['act'] == 'detail_page'){
					$single_ad=$digital_model->single_digital_ad($p_id);
					$digital_view->aspk_show_detail_view($single_ad);
				}
			}else{
				$end_limit=25;
				$want_list_ads=$digital_model->get_ads_list_for_user($user_id,$values,$end_limit);
				if(count($want_list_ads) < 1){
				?>
				<div class="tw-bs container" style="min-height:30em">
					<div class="row">
						<div class="col-md-12 digital_status" >
							Currently there are no items available. Please check back later
						</div>
					</div>
				</div>
				<?php
				}else{
					$want_list_count_ads= count($want_list_ads);
					$digital_view->aspk_show_list_view($want_list_ads,$want_list_count_ads,$game_values,$end_limit);
				}
			}
		}
		
		function handle_comunication_form(){
			$dgt_m = new Aspk_Digital_Ecommerce_Model();
			
			$seller_name = $_POST['aspk_seler_name'];
			$ad_id = $_POST['aspk_ad_id'];
			$msg = $_POST['aspk_buyer_message'];
			$buyer_name = $_POST['aspk_buyer_name'];
			$aspk_product_id = $_POST['aspk_product_idd'];
			$date_time = date('Y-m-d H:i:s');
			$m_flow = $dgt_m->get_msg_flow($ad_id); 
			$b_fname = get_user_meta( get_current_user_id() , 'first_name',true);
			$b_lname = get_user_meta( get_current_user_id() , 'last_name',true);
			$ful_name = $b_fname.' '.$b_lname;
			if($m_flow->buyer_uid == $buyer_name){
				$color = '#FDE7E7';
			}else{
				$color ='#F5CC99';
			}
			if(count($m_flow->msg_flow) < 1){
				ob_start();
				?>
				<div style="margin-top:5px;clear:left;padding:1em;border-radius:10px;background-color:<?php echo $color; ?>">
					<div class="row">
						<div class="col-md-12">
							<b><?php echo $ful_name.' '.$date_time;?></b>
							<p><?php echo $msg;?></p>
						</div>
					</div>
				</div>
				<?php
				$dgt_msg = ob_get_clean();
				$dgt_m->insert_conf_into_d_com_msg($ad_id,$seller_name,$buyer_name,$dgt_msg,$date_time,$date_time);
			}else{
				ob_start();
				?>
				<div class="row">
					<div class="col-md-12"><?php echo $m_flow->msg_flow; ?></div>
				</div>
				<div style="clear:left;padding:1em;border-radius:10px;background-color:#FDE7E7;">
					<div class="row">
						<div class="col-md-12">
							<b><?php echo $ful_name.' '.$date_time;?></b>
							<p><?php echo $msg;?></p>
						</div>
					</div>
				</div>
				<?php
				$dgt_msg_send = ob_get_clean();
				$dgt_m->update_conv_by_ad_id($ad_id,$dgt_msg_send,$date_time);
			}
		}	

		
	}// class ends
	
}//if ends

new Aspk_Digital_Ecommerce();
